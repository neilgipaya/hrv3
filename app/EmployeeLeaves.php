<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeaves extends Model
{
    //

    protected $fillable = ['users_id', 'leave_type_code', 'approver_id', 'from_date', 'to_date', 'reason', 'remarks', 'leave_without_pay',
        'half_day', 'leave_amount', 'status', 'filling_date', 'total_hours'];


    public function users(){

        return $this->belongsTo('App\User');
    }

}
