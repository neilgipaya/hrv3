<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  EmployeeJobDetails extends Model
{
    //

    protected $fillable = ['users_id', 'job_title', 'departments_id', 'cost_centers_id', 'pay_groups_id', 'hire_date', 'employment_status',
        'employment_effectivity_date', 'work_hours_per_day', 'tin_number', 'sss_number', 'hdmf_number', 'philhealth_number', 'companies_id',
        'date_resign', 'work_email'];

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function department(){

        return $this->belongsTo('App\Departments', 'departments_id');
    }

    public function costCenter(){

        return $this->belongsTo('App\CostCenter', 'cost_centers_id');
    }

    public function company(){

        return $this->belongsTo('App\Companies', 'companies_id');
    }


}
