<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DTR extends Model
{
    //

    protected $table = "dtr";

    protected $guarded = [];

    public function employee(){

        return $this->belongsTo('App\User', 'users_id');
    }


}
