<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    //

    protected $fillable = ['companies_id', 'announcement_title', 'announcement_description', 'start_date', 'end_date', 'attachment'];

    public function company(){

        return $this->belongsTo('App\Companies', 'companies_id');
    }
}
