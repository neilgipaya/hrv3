<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingHeader extends Model
{
    //

    protected $guarded = [];

    public function company(){

        return $this->belongsTo('App\Companies', 'company_id');
    }
}
