<?php


namespace App\Services;


use App\EmployeeLeaves;
use App\EmploymentInformation;
use App\ReportMethod;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LeaveService
{

    public function processLeave($request){

        DB::beginTransaction();

        try {

            $leave_balance = $this->checkBalance($request->post('leave_type_code'));

            if ($leave_balance < $request->post('leave_amount')){
                if ($request->has('update_leave')){
                    $notification = array(
                        'message' => 'Insufficient Leave Balance',
                        'alert-type' => 'error',
                    );

                    return $notification;
                }
                return response()->json(['message'=>'Insufficient Leave Balance', 'type'=>'error']);
            }

            //deduct the leave because it's paid
            if ($request->post('leave_without_pay') === "1"){

                $this->deductLeave($request->post('leave_amount'), $request->post('leave_type_code'));

            } else {

                $request->merge([
                    'total_hours' => 0
                ]);

            }


            if ($request->has('update_leave')){

                EmployeeLeaves::find($request->post('id'))->update($request->except('_token', '_method', 'leave_schedule', 'update_leave'));

            } else {


                EmployeeLeaves::create($request->all());

            }

            DB::commit();

            if ($request->has('update_leave')){
                $notification = array(
                    'message' => 'Leave Updated Successfully',
                    'alert-type' => 'success',
                );

                return $notification;
            }


            return response()->json(['message'=>'Leave Applied Successfully', 'type'=>'success']);


        } catch (\Exception $e){
            DB::rollBack();
            dd($e);
        }



    }

    public function processLeaveUpdate($request){

        DB::beginTransaction();

        try {

            $leave_balance = $this->checkBalance($request->get('leave_type_code'));

            if ($leave_balance < $request->get('leave_amount')){

                return response()->json(['message'=>'Insufficient Leave Balance', 'type'=>'error']);
            }

            //deduct the leave because it's paid
            if ($request->post('without_pay') === "1"){
                $this->deductLeave($request->get('leave_amount'), $request->get('leave_type_code'));
            } else {
                $request->merge([
                    'total_hours' => 0
                ]);
            }

            EmployeeLeaves::updateOrCreate($request->all());
            DB::commit();

            return response()->json(['message'=>'Leave Applied Successfully', 'type'=>'success']);


        } catch (\Exception $e){
            DB::rollBack();
            dd($e);
        }



    }



    private function checkBalance($leaveType){

        $leave_balance = EmploymentInformation::where('users_id', Auth::user()->id)->pluck($leaveType)->first();

        return $leave_balance;

    }

    private function deductLeave($leaveAmount, $column){
        DB::table('employment_information')
            ->where('users_id', \auth()->user()->id)
            ->decrement($column, $leaveAmount);

    }

}
