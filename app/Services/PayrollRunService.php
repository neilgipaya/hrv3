<?php


namespace App\Services;


use App\Http\Controllers\Formatter\StringFormatter;
use App\OTRates;
use App\PayrollRuns;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PayrollRunService
{

    /**
     * @var AttendanceService
     */
    private $attendanceService;
    /**
     * @var EmployeeService
     */
    private $employeeService;
    /**
     * @var StringFormatter
     */
    private $stringFormatter;

    /**
     * PayrollRunService constructor.
     * @param  AttendanceService  $attendanceService
     * @param  EmployeeService  $employeeService
     * @param  StringFormatter  $stringFormatter
     */
    public function __construct(
        AttendanceService $attendanceService,
        EmployeeService $employeeService,
        StringFormatter $stringFormatter
    )
    {
        $this->attendanceService = $attendanceService;
        $this->employeeService = $employeeService;
        $this->stringFormatter = $stringFormatter;
    }

    public function calculateCompanyAttendance($company_id){

        //
    }

    /**
     * Calculate Employee Attendance from DTR PROCESS
     * @param $employeeArray
     * @param $start_date
     * @param $end_date
     * @param $options
     * @return array
     */
    public function calculateEmployeePayroll($employeeArray, $start_date, $end_date, $options, $header_id){

        $payroll_register = [];

        foreach ($employeeArray as $employee){

            $attendance_data = $this->attendanceService->getCalculatedAttendance($employee, $start_date, $end_date);

            $employee_data = User::find($employee);

            if (empty($attendance_data)){

                $data['status'] = "error";
            } else {

                $data['status'] = "success";
            }

            $monthly_salary = intval($this->stringFormatter->cleanComma($employee_data->salaries->basic_salary));

            if ($monthly_salary > 0) {

                $cut_off_salary = (float) $monthly_salary / 2;
                $daily_salary = (float) ($monthly_salary * 12) / 261;
                $hourly_salary = (float) $daily_salary / 8;

                $data['rd_hours'] = $this->computeHours($attendance_data->rd_hours, $hourly_salary, "Rest Day", "regular");
                $data['sh_hours'] = $this->computeHours($attendance_data->sh_hours, $hourly_salary, "Special Holiday",
                    "regular");
                $data['lh_hours'] = $this->computeHours($attendance_data->lh_hours, $hourly_salary, "Legal Holiday",
                    "regular");
                $data['sh_rd_hours'] = $this->computeHours($attendance_data->sh_rd_hours, $hourly_salary,
                    "Special Holiday Rest Day", "regular");
                $data['lh_rd_hours'] = $this->computeHours($attendance_data->lh_rd_hours, $hourly_salary,
                    "Legal Holiday Rest Day", "regular");
                $data['dh_hours'] = $this->computeHours($attendance_data->dh_hours, $hourly_salary, "Double Holiday",
                    "regular");
                $data['dh_rd_hours'] = $this->computeHours($attendance_data->dh_rd_hours, $hourly_salary,
                    "Double Holiday Rest Day", "regular");
                $data['overtime'] = $this->computeHours($attendance_data->overtime, $hourly_salary, "Ordinary", "ot");
                $data['rd_ot_hours'] = $this->computeHours($attendance_data->rd_ot_hours, $hourly_salary, "Rest Day",
                    "ot");
                $data['sh_ot_hours'] = $this->computeHours($attendance_data->sh_ot_hours, $hourly_salary,
                    "Special Holiday", "ot");
                $data['lh_ot_hours'] = $this->computeHours($attendance_data->lh_ot_hours, $hourly_salary,
                    "Legal Holiday", "ot");
                $data['sh_rd_ot_hours'] = $this->computeHours($attendance_data->sh_rd_ot_hours, $hourly_salary,
                    "Special Holiday Rest Day", "ot");
                $data['lh_rd_ot_hours'] = $this->computeHours($attendance_data->lh_rd_ot_hours, $hourly_salary,
                    "Legal Holiday Rest Day", "ot");
                $data['dh_ot_hours'] = $this->computeHours($attendance_data->dh_ot_hours, $hourly_salary,
                    "Double Holiday", "ot");
                $data['dh_rd_ot_hours'] = $this->computeHours($attendance_data->dh_rd_ot_hours, $hourly_salary,
                    "Double Holiday Rest Day", "ot");


                $total_additional = array_sum($data);


                $data['basic_hours'] = $attendance_data->basic_hours;
                $data['late'] = $attendance_data->late;
                $data['undertime'] = $attendance_data->undertime;


                $total_gross = $attendance_data->basic_hours * $hourly_salary;
                $total_gross = ($total_gross + $total_additional) - ($attendance_data->late + $attendance_data->undertime);

                $data['total_gross'] = $total_gross;

                if ($options['compute_sss'] === "Yes") {

                    $data['sss_deduction'] = $this->getSSSContribution($monthly_salary)->ss_ee;

                } else {

                    $data['sss_deduction'] = 0;

                }

                if ($options['compute_pagibig'] === "Yes") {
                    $data['pagibig_deduction'] = 100;
                } else {

                    $data['pagibig_deduction'] = 0;

                }

                if ($options['compute_philhealth'] === "Yes") {

                    $data['philhealth_deduction'] = $this->getPhilhealthDeduction($monthly_salary)->employee_share;

                } else {

                    $data['philhealth_deduction'] = 0;

                }

                $total_contribution = $data['sss_deduction'] + $data['pagibig_deduction'] + $data['philhealth_deduction'];

                $data['taxable_income'] = $total_gross - $total_contribution;

                if ($options['compute_tax'] === "Yes") {

                    $tax = $this->getWitholdingTax($data['taxable_income'], "MONTHLY");
                    if (isset($tax) || !empty($tax)) {
                        $salary_excess = $data['taxable_income'] - $tax->salary;
                        $salary_excess = $salary_excess * $tax->percentage_over;
                        $withholding_tax = $salary_excess + $tax->tax;

                        $data['withholding_tax'] = $withholding_tax;
                    } else {

                        $data['withholding_tax'] = 0;
                    }


                } else {

                    $data['withholding_tax'] = 0;
                }

                $total_deductions = $total_contribution + $data['withholding_tax'];

                $data['total_net_pay'] = $total_gross - $total_deductions;

                $data['user_id'] = $attendance_data->user_id;
                $data['monthly_salary'] = $monthly_salary;
                $data['daily_salary'] = $daily_salary;
                $data['hourly_rate'] = $hourly_salary;
                $data['payroll_header_id'] = $header_id;

                $this->savePayrollRegister($data);
            }


        }

        return $payroll_register;

    }


    private function getWitholdingTax($total_gross, $type){

        $query = DB::table('d_withholding_tax')
            ->where('salary', '<=', $total_gross)
            ->where('pay_schedule', '=', $type)
            ->orderByDesc('salary')
            ->first();

        return $query;
    }

    private function getSSSContribution($total_gross){

        $query = DB::table('d_sss')
            ->where('salary_range_from', '<=', $total_gross)
            ->where('salary_range_to', '>=', $total_gross)
            ->first();

        return $query;

    }

    private function getPhilhealthDeduction($total_gross){

        $query = DB::table('d_philhealth')
            ->where('salary_range_from', '<=', $total_gross)
            ->where('salary_range_to', '>=', $total_gross)
            ->first();

        return $query;

    }

    private function computeHours($hours, $rate, $code, $type) : float {

        $payrollRate = $this->getPolicy($code, $type);

        $rate = $rate * $payrollRate;
        $result = $hours * $rate;

        return floatval($result);

    }

    private function getPolicy($code, $type){

        $query = OTRates::all()->where('code', $code)->pluck($type);

        if (isset($query)){
            return $query[0];
        }

        return [];

    }

    private function savePayrollRegister($data){

        unset($data['status']);

        PayrollRuns::updateOrCreate($data);
    }

}
