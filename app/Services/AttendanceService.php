<?php


namespace App\Services;


use App\AttendanceRecord;
use App\DTR;
use App\EmployeeLeaves;
use App\Http\Controllers\Formatter\DateFormatter;
use App\OTRates;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AttendanceService
{
    /**
     * @var DateFormatter
     */
    private $dateFormatter;

    /**
     * AttendanceService constructor.
     * @param  DateFormatter  $dateFormatter
     */
    public function __construct(DateFormatter $dateFormatter)
    {
        $this->dateFormatter = $dateFormatter;
    }

    /**
     * @param $company_employees
     * @return array
     */
    public function computeHours($company_employees, $from, $to) {

        $attendance_record = [];

        foreach ($company_employees as $employee){

            $data['user_id'] = $employee->users_id;
            $data['attendance_from'] = $from;
            $data['attendance_to'] = $to;

            //calculate basic hours
            $attendance_hours = $this->calculateBasicHours($employee->users_id, $from, $to);

            $approved_leave = $this->getApprovedLeave($employee->users_id, $from, $to);
            $approved_ob = $this->getApprovedOB($employee->users_id, $from, $to);
            $basic_hours = array_sum(array_column($attendance_hours, 'total_work'));

            $basic_hours_total = $basic_hours + $approved_ob + $approved_leave;

            $data['basic_hours'] = $basic_hours_total;
            $data['late'] = array_sum(array_column($attendance_hours, 'tardiness'));
            $data['undertime'] = array_sum(array_column($attendance_hours, 'overtime'));
            $data['rd_hours'] = array_sum(array_column($attendance_hours, 'rd_hours'));

            //TODO : CALCULATE OTHER WORK HOURS


            $this->saveDTR($data);
        }

        return $attendance_record;
    }

    public function getApprovedOT($employee_id, $start_date, $end_date){

        $query = DB::table('employee_overtimes')
            ->whereBetween('filling_date', array($start_date, $end_date))
            ->where('users_id', $employee_id)
            ->where('status', 'Approved')
            ->sum('total_hours');

        if (isset($query)){
            return $query;
        }

        return 0;

    }

    public function getCalculatedAttendance($employee_id, $from, $to){

        $query = DB::table('attendance_records')
            ->where('attendance_from', '>=', $from)
            ->where('attendance_to', '<=', $to)
            ->where('user_id', $employee_id)
            ->first();


        return $query;

    }

    private function calculateBasicHours($employee_id, $from, $to) : array {

        $attendances = $this->getAllAttendance($employee_id, $from, $to);

        $basic_hours = [];

        foreach ($attendances as $attendance){

            $q['total_work'] = $attendance->total_work;
            $q['tardiness'] = $attendance->tardiness;
            $q['undertime'] = $attendance->undertime;

            //calculate rest day hours

            if (Carbon::parse($attendance->attendance_date)->isSaturday() || Carbon::parse($attendance->attendance_date)->isSunday()){
                $q['rd_hours'] = $attendance->total_work;
            } else {
                $q['rd_hours'] = 0;
            }

            array_push($basic_hours, $q);
        }

        return $basic_hours;


    }



    private function getApprovedOB($employee_id, $start_date, $end_date){

        $query = DB::table('employee_obs')
            ->whereBetween('filling_date', array($start_date, $end_date))
            ->where('users_id', $employee_id)
            ->where('status', 'Approved')
            ->sum('num_hours');

        if (isset($query)){
            return $query;
        }

        return 0;

    }


    /**
     * @param $employee_id
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    private function getApprovedLeave($employee_id, $start_date, $end_date) : float {

        $query = DB::table('employee_leaves')
            ->whereBetween('filling_date', array($start_date, $end_date))
            ->where('users_id', $employee_id)
            ->where('status', 'Approved')
            ->sum('total_hours');

        if (isset($query)){
            return $query;
        }

        return 0;

    }




    /**
     * @param $employee_id
     * @param $date_from
     * @param $date_to
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object
     */
    private function getAllAttendance($employee_id, $date_from, $date_to){

        $query = DB::table('attendance_time')
            ->whereBetween('attendance_date', array($date_from, $date_to))
            ->where('users_id', $employee_id)
            ->get();

        return $query;


    }

    private function saveDTR($data){

        AttendanceRecord::updateOrCreate($data);

    }
}
