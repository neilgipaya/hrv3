<?php


namespace App\Services;


use App\AttendanceRecord;
use App\Billing;
use App\DTR;
use App\EmployeeLeaves;
use App\EmployeeSalary;
use App\Http\Controllers\Formatter\DateFormatter;
use App\Http\Controllers\Formatter\StringFormatter;
use App\OTRates;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class BillingService
{
    /**
     * @var DateFormatter
     */
    private $dateFormatter;
    /**
     * @var StringFormatter
     */
    private $stringFormatter;

    /**
     * AttendanceService constructor.
     * @param  DateFormatter  $dateFormatter
     * @param  StringFormatter  $stringFormatter
     */
    public function __construct(DateFormatter $dateFormatter, StringFormatter $stringFormatter)
    {
        $this->dateFormatter = $dateFormatter;
        $this->stringFormatter = $stringFormatter;
    }

    /**
     * @param $company_employees
     * @return array
     */
    public function computeBilling($billing_header, $company_employees, $from, $to, $company_id) {

        $attendance_record = [];

        foreach ($company_employees as $employee){

            $data['billing_header'] = $billing_header;
            $data['user_id'] = $employee->users_id;

            $salaryDetails = $this->getSalary($employee->users_id);
            $monthly_salary = intval($this->stringFormatter->cleanComma($salaryDetails->basic_salary));

            if ($monthly_salary > 0){
//                $cut_off_salary = (float) $monthly_salary / 2;
//                $daily_salary = (float) ($monthly_salary * 12) / 261;
//                $hourly_salary = (float) $daily_salary / 8;
                $cut_off_salary = (float) $monthly_salary / 2;
                $daily_salary = 373;
                $hourly_salary = 46.625;


                $attendance_hours =$this->calculateBasicHours($employee->users_id, $from, $to);

                $data['daily_rate'] = $daily_salary;
                $data['rh'] = array_sum(array_column($attendance_hours, 'total_work'));
                $data['php_rh'] = $data['rh'] * $hourly_salary;
                $data['late'] = array_sum(array_column($attendance_hours, 'tardiness'));
                $data['php_late'] = round($hourly_salary * $data['late'], 2);
                $data['absent'] = 0;
                $data['php_absent'] = 0;
                $data['undertime'] = array_sum(array_column($attendance_hours, 'undertime'));
                $data['php_undertime'] = round($hourly_salary * $data['undertime'], 2);

                $data['basic_pay'] = $data['php_rh'] - $data['php_late'] - $data['php_absent'] - $data['php_undertime'];

                $data['nsd'] = 0;
                $data['php_nsd'] = 0;
                $data['nsd2'] = 0;

                $data['php_nsd2'] = 0;
                $data['rot'] = array_sum(array_column($attendance_hours, 'overtime'));

                $ot_rate = $hourly_salary * 1.25;
                $rdot_rate = $hourly_salary * 1.30;
                $data['php_rot'] = $ot_rate * $data['rot'];

                $data['rdot'] = array_sum(array_column($attendance_hours, 'rd_hours'));
                $data['php_rdot'] = $rdot_rate * $data['rdot'];

                $data['rdote'] = array_sum(array_column($attendance_hours, 'rd_ot'));
                $data['php_rdote'] = $rdot_rate * $data['rdote'];

                $data['shot'] = 0;
                $data['php_shot'] = 0;

                $data['shote'] = 0;
                $data['php_shote'] = 0;

                $data['shrd'] = 0;
                $data['php_shrd'] = 0;

                $data['shrde'] = 0;
                $data['php_shrde'] = 0;

                $data['lhot'] = 0;
                $data['php_lhot'] = 0;

                $data['lhote'] = 0;
                $data['php_lhote'] = 0;

                $data['lhrd'] = 0;
                $data['php_lhrd'] = 0;

                $data['lhrde'] = 0;
                $data['php_lhrde'] = 0;

                $data['holiday'] = 0;
                $data['php_holiday'] = 0;

                $data['php_total_ot'] = $data['php_rot'] + $data['php_rdot'] + $data['php_lhot'] + $data['php_lhote'] + $data['php_shot'] + $data['php_shote'];

                $data['cola'] = $salaryDetails->cola;

                $data['gross_pay'] = $data['basic_pay'] + $data['php_total_ot'] + $data['php_nsd'] + $data['php_nsd2'] + $data['php_shrd'] + $data['php_shrde'] + $data['php_lhrd'] + $data['php_lhrde'] + $data['php_holiday'];

                $data['er_sss'] = $this->getSSSContribution($data['gross_pay'])->ss_er;
                $data['er_ec'] = $this->getSSSContribution($data['gross_pay'])->ec_er;
                $data['er_phic'] = $this->getPhilhealthDeduction($data['gross_pay'])->employer_share;
                $data['er_hdmf'] = 100;
                $data['allowance'] = $salaryDetails->allowance;
                $data['total_labor'] = $data['gross_pay'] + $data['er_sss'] + $data['er_ec'] + $data['er_phic'] + $data['er_hdmf'] + $data['allowance'];
                $data['sil'] = 77.71;
                $data['th_month'] = 0;
                $data['separation'] = 0;
                $data['ppe'] = 0;
                $data['total'] = $data['total_labor'] + $data['sil'] + $data['th_month'] + $data['separation'] + $data['ppe'];
                $data['admin'] = 0;
                $data['grand_total'] = $data['total'] + $data['admin'];
            }


            $this->saveBilling($data);

            array_push($attendance_record, $data);
        }

        return $attendance_record;
    }

    public function getSalary($employee_id){

        $salary = EmployeeSalary::where('users_id', $employee_id)->first();

        if (isset($salary)){
            return $salary;
        }

        return [];

    }

    private function getSSSContribution($total_gross){

        $query = DB::table('d_sss')
            ->where('salary_range_from', '<=', $total_gross)
            ->where('salary_range_to', '>=', $total_gross)
            ->first();

        return $query;

    }

    private function getPhilhealthDeduction($total_gross){

        $query = DB::table('d_philhealth')
            ->where('salary_range_from', '<=', $total_gross)
            ->where('salary_range_to', '>=', $total_gross)
            ->first();

        return $query;

    }

    public function getApprovedOT($employee_id, $start_date, $end_date){

        $query = DB::table('employee_overtimes')
            ->whereBetween('filling_date', array($start_date, $end_date))
            ->where('users_id', $employee_id)
            ->where('status', 'Approved')
            ->sum('total_hours');

        if (isset($query)){
            return $query;
        }

        return 0;

    }

    public function getCalculatedAttendance($employee_id, $from, $to){

        $query = DB::table('attendance_records')
            ->where('attendance_from', '>=', $from)
            ->where('attendance_to', '<=', $to)
            ->where('user_id', $employee_id)
            ->first();


        return $query;

    }

    private function calculateBasicHours($employee_id, $from, $to) : array {

        $attendances = $this->getAllAttendance($employee_id, $from, $to);

        $basic_hours = [];

        foreach ($attendances as $attendance){

            $q['total_work'] = $attendance->total_work;
            $q['tardiness'] = $attendance->tardiness;
            $q['undertime'] = $attendance->undertime;
            $q['overtime'] = $attendance->overtime;

            //calculate rest day hours

            if (Carbon::parse($attendance->attendance_date)->isSaturday() || Carbon::parse($attendance->attendance_date)->isSunday()){
                $q['rd_hours'] = $attendance->total_work;
                $q['rd_ot'] = $attendance->overtime;
            } else {
                $q['rd_hours'] = 0;
                $q['rd_ot'] = 0;
            }


            array_push($basic_hours, $q);
        }

        return $basic_hours;


    }



    private function getApprovedOB($employee_id, $start_date, $end_date){

        $query = DB::table('employee_obs')
            ->whereBetween('filling_date', array($start_date, $end_date))
            ->where('users_id', $employee_id)
            ->where('status', 'Approved')
            ->sum('num_hours');

        if (isset($query)){
            return $query;
        }

        return 0;

    }


    /**
     * @param $employee_id
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    private function getApprovedLeave($employee_id, $start_date, $end_date) : float {

        $query = DB::table('employee_leaves')
            ->whereBetween('filling_date', array($start_date, $end_date))
            ->where('users_id', $employee_id)
            ->where('status', 'Approved')
            ->sum('total_hours');

        if (isset($query)){
            return $query;
        }

        return 0;

    }




    /**
     * @param $employee_id
     * @param $date_from
     * @param $date_to
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object
     */
    private function getAllAttendance($employee_id, $date_from, $date_to){

        $query = DB::table('attendance_time')
            ->whereBetween('attendance_date', array($date_from, $date_to))
            ->where('users_id', $employee_id)
            ->get();

        return $query;


    }

    private function saveBilling($data){

        Billing::updateOrCreate($data);

    }
}
