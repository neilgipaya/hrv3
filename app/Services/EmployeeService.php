<?php


namespace App\Services;


use App\EmployeeJobDetails;
use App\EmployeeSalary;
use App\EmploymentInformation;
use App\User;
use Illuminate\Support\Facades\DB;

class EmployeeService
{


    public function make($request){

        DB::beginTransaction();

        try {

            $userService = $this->createUsers($request);

            $request->merge([
                'users_id' => $userService->id
            ]);

            $salaryService = $this->createSalary($request);

            $jobService = $this->createJobDetails($request);

            $employmentService = $this->employmentService($request);

            $userService->assignRole($request->post('role'));

            DB::commit();

            return true;

        } catch (\Exception $exception){
            DB::rollBack();

            dd($exception);
            return false;
        }

    }


    public function getEmployeesByCompany($company_id){

        try{

            $query = DB::table('users')
                ->join('employee_job_details', 'users.id', '=', 'employee_job_details.users_id')
                ->where('employee_job_details.companies_id', $company_id)
                ->get();

            return $query;

        } catch (\Exception $e){

        }

    }

    public function getEmployeeById($users_id){

        $query = User::find($users_id);

        if (isset($query)){
            return $query;
        }

        return [];

    }

    private function createUsers($request){

        $users = User::create($request->all());

        return $users;
    }

    private function createSalary($request){

        $salary = EmployeeSalary::create($request->all());

        return $salary;

    }

    private function createJobDetails($request){

        $jobDetails = EmployeeJobDetails::create($request->all());

        return $jobDetails;

    }

    private function employmentService($request){

        $employment = EmploymentInformation::create($request->all());

        return $employment;

    }
}
