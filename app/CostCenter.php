<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostCenter extends Model
{
    //

    use SoftDeletes;

    protected $guarded = [];

    public function jobDetails(){

        return $this->hasOne('App\EmployeeJobDetails');
    }
}
