<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeOB extends Model
{
    //

    protected $table = "employee_obs";

    protected $guarded = [];


    public function users(){

        return $this->belongsTo('App\User');
    }

}
