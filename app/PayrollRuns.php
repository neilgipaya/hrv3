<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollRuns extends Model
{
    //

    protected $table = "payroll_runs";

    protected $guarded = [];
}
