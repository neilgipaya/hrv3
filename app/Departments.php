<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departments extends Model
{
    //

    use SoftDeletes;

    protected $guarded = [];

    public function jobDetails(){

        return $this->hasOne('App\EmployeeJobDetails');
    }

    public function company(){
       return $this->belongsTo('App\Companies', 'companies_id');
    }
}
