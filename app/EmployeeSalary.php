<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    //

    protected $fillable = ['users_id', 'tax_code', 'is_minimum_earner', 'cola', 'work_days_per_year', 'basic_salary', 'de_minimis', 'ot_computation_code',
        'sss_contribution', 'philhealth_contribution', 'hdmf_contribution', 'additional_contribution'];

    public function user(){

        return $this->belongsTo('App\User');
    }
}
