<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentInformation extends Model
{
    // for leaves

    protected $fillable = ['users_id', 'vl', 'sl', 'other_leaves', 'maternity', 'paternity', 'birthday', 'service', 'parental', 'rehabilitation', 'study'];

    public function user(){

        return $this->belongsTo('App\User');
    }
}
