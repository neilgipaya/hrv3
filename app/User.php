<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['employee_id', 'lastname', 'firstname', 'middlename', 'gender', 'date_of_birth', 'civil_status', 'contact_no', 'home_address',
        'zip_code', 'username', 'email', 'password', 'enable_login', 'suffix', 'nationality', 'religion'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function jobDetails(){

        return $this->hasOne('App\EmployeeJobDetails', 'users_id');
    }

    public function employments(){

        return $this->hasOne('App\EmploymentInformation', 'users_id');
    }

    public function salaries(){

        return $this->HasOne('App\EmployeeSalary', 'users_id');
    }

    public function dtr(){

        return $this->hasOne('App\DTR');
    }

    public function leaves(){

        return $this->hasMany('App\EmployeeLeaves', 'users_id');
    }

    public function overtime(){

        return $this->hasMany('App\EmployeeOvertime', 'users_id');
    }

    public function approver(){
        return $this->hasOne('App\ReportMethod');
    }

    public function subordinates(){
        return $this->hasMany('App\ReportMethod', 'users_id');
    }

    public function ob()
    {
        return $this->hasOne('App\EmployeeOB', 'users_id');
    }

    public function billing(){

        return $this->hasOne('App\Billing', 'users_id');
    }

    public function attendance(){

        return $this->hasMany('App\AttendanceTime');
    }

    public function attendances(){

        return $this->hasMany('App\AttendanceTime');
    }

}
