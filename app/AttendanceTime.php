<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceTime extends Model
{
    //

    protected $table = "attendance_time";

    protected $guarded = [];

    public function employee(){

        return $this->belongsTo('App\User', 'users_id');
    }
}
