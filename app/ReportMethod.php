<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportMethod extends Model
{

    protected $fillable = ['users_id', 'approver_id', 'reporting_type'];

    public function approver(){

        return $this->belongsTo('App\User', 'approver_id');

    }
//
//    public function approver(){
//
//        return $this->hasOne('App\User', 'approver_id');
//    }


}
