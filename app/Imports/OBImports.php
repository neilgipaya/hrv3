<?php


namespace App\Imports;


use App\EmployeeOB;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Row;

class OBImports implements OnEachRow, WithStartRow
{

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }



    /**
     * @param  Row  $row
     * @throws \Exception
     */
    public function onRow(Row $row)
    {
        $row = $row->toArray();

        $ob = EmployeeOB::create([

            'users_id' => $row[0],
            'approver_id' => $row[1],
            'filling_date'=>Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]))->format('Y-m-d'),
            'start_date'=>Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3]))->format('Y-m-d'),
            'end_date'=>Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[4]))->format('Y-m-d'),
            'start_time' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5]))->format('H:i:s'),
            'end_time' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[6]))->format('H:i:s'),
            'num_hours' => $row[7],
            'num_days' => $row[8],
            'purpose' => $row[9],
            'travel_distance' => $row[10],
            'transportation_amount' => $row[11],
            'grab' => $row[12],
            'meals' => $row[13],
            'others' => $row[14],
            'reason' => $row[15],
            'status' => $row[16]
        ]);


    }
}
