<?php


namespace App\Imports;


use App\EmployeeOvertime;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Row;

class OvertimeImport implements OnEachRow, WithStartRow
{

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param  Row  $row
     * @throws \Exception
     */
    public function onRow(Row $row)
    {
        $row = $row->toArray();

        $overtime = EmployeeOvertime::create([
            'users_id' => $row[0],
            'approver_id' => $row[1],
            'time_in' =>  Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]))->format('H:i s'),
            'time_out' =>  Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3]))->format('H:i s'),
            'remarks' => $row[4],
            'filling_date' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5]))->format('Y-m-d'),
            'status' => $row[6]
        ]);


    }


}
