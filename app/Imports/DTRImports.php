<?php


namespace App\Imports;


use App\DTR;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Row;

class DTRImports implements OnEachRow
{


    /**
     * @param  Row  $row
     * @throws \Exception
     */
    public function onRow(Row $row)
    {
        $rowIndex = $row->getIndex();
        $row = $row->toArray();
//        dd($row);

        $dtr = DTR::create([
            'users_id' => $row[0],
            'attendance_date' => $row[1],
            'time_in' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]))->format('H:i:s'),
            'time_out' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3]))->format('H:i:s'),
            'late' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[4]))->format('H:i:s'),
            'undertime' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5]))->format('H:i:s'),
            'overtime' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[6]))->format('H:i:s'),
            'total_work_hours' => $row['7'],
        ]);


    }
}
