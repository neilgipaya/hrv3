<?php


namespace App\Imports;


use App\EmployeeLeaves;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Row;

class LeaveImports implements OnEachRow, WithStartRow
{

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param  Row  $row
     * @throws \Exception
     */
    public function onRow(Row $row)
    {
        $row = $row->toArray();

        $leave = EmployeeLeaves::create([
            'users_id' => $row[0],
            'leave_type_code' => $row[1],
            'approver_id' => $row[2],
            'from_date' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3]))->format('Y-m-d'),
            'to_date' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[4]))->format('Y-m-d'),
            'reason' => $row[5],
            'remarks' =>$row[6],
            'leave_without_pay' =>  number_format($row[7]),
            'half_day' => number_format($row[8]),
            'is_deducted' => number_format($row[9]),
            'leave_amount' => $row[10],
            'status' => $row[11],
            'filling_date' => Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[12]))->format('Y-m-d')
        ]);


    }


}
