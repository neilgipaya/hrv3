<?php

namespace App\Http\Controllers;

use App\EmployeeJobDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeJobDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeJobDetails  $employeeJobDetails
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeJobDetails $employeeJobDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeJobDetails  $employeeJobDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeJobDetails $employeeJobDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            EmployeeJobDetails::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Job details updated successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeJobDetails  $employeeJobDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeJobDetails $employeeJobDetails)
    {
        //
    }
}
