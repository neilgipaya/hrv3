<?php

namespace App\Http\Controllers;

use App\TaxStatus;
use Illuminate\Http\Request;

class TaxStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaxStatus  $taxStatus
     * @return \Illuminate\Http\Response
     */
    public function show(TaxStatus $taxStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaxStatus  $taxStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(TaxStatus $taxStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaxStatus  $taxStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaxStatus $taxStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaxStatus  $taxStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaxStatus $taxStatus)
    {
        //
    }
}
