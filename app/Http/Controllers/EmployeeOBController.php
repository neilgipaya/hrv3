<?php

namespace App\Http\Controllers;

use App\EmployeeOB;
use App\Http\Controllers\Formatter\DateFormatter;
use App\Http\Controllers\Helper\TimesheetHelper;
use App\Imports\OBImports;
use App\Imports\OvertimeImport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeOBController extends Controller
{
    /**
     * @var DateFormatter
     */
    private $dateFormatter;
    /**
     * @var TimesheetHelper
     */
    private $timesheetHelper;


    /**
     * EmployeeOBController constructor.
     * @param  DateFormatter  $dateFormatter
     * @param  TimesheetHelper  $timesheetHelper
     */
    public function __construct(DateFormatter $dateFormatter, TimesheetHelper $timesheetHelper)
    {
        $this->dateFormatter = $dateFormatter;
        $this->timesheetHelper = $timesheetHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['obs'] = EmployeeOB::all()->where('users_id', auth()->user()->id);

        return view('Timesheet.OB.employee-ob', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $approvers = $this->timesheetHelper->checkApprovers();

        if (!isset($approvers) || empty($approvers)){

            return redirect()->back()->with(['message' => 'You currently dont have any approver', 'type' => 'error']);
        }


        DB::beginTransaction();

        try{

            if ($request->hasFile('file_attachment')) {
                $filename = time().$request->file('file_attachment')->getClientOriginalName();

                Storage::disk('public_uploads')->putFileAs(
                    'attachments/',
                    $request->file('file_attachment'),
                    $filename
                );

                $request->merge([
                    'attachment' => $filename
                ]);
            }


            $request->merge([
                'users_id' => auth()->user()->id,
                'approver_id' => $approvers,
                'num_hours' => $this->dateFormatter->diffInHours([
                    $request->post('start_time'),
                    $request->post('end_time')
                ]),
                'num_days' => $this->dateFormatter->diffInDate([
                    $request->post('start_date'),
                    $request->post('end_date')
                ]),
                'filling_date' => Carbon::now()->format('Y-m-d'),
                'status' => "Pending"
            ]);


            EmployeeOB::create($request->except('file_attachment'));

            DB::commit();

            $notification = array(
                'message' => 'OB Filled Successfully',
                'alert-type' => 'success',
            );

            return redirect()->back()->with($notification);

        } catch (\Exception $e){
            DB::rollBack();

            dd($e);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeOB  $employeeOB
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeOB $employeeOB)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeOB  $employeeOB
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeOB $employeeOB)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try{

            if ($request->has('approval')){

                EmployeeOB::find($id)->update($request->except('approval'));

                $notification = array(
                    'message' => 'OB Updated Successfully',
                    'alert-type' => 'info',
                );

                return redirect()->back()->with($notification);
            }

            if ($request->hasFile('file_attachment')) {
                $filename = time().$request->file('file_attachment')->getClientOriginalName();

                Storage::disk('public_uploads')->putFileAs(
                    'attachments/',
                    $request->file('file_attachment'),
                    $filename
                );

                $request->merge([
                    'attachment' => $filename
                ]);
            }


            $request->merge([
                'users_id' => auth()->user()->id,
                'approver_id' => $approvers,
                'num_hours' => $this->dateFormatter->diffInHours([
                    $request->post('start_time'),
                    $request->post('end_time')
                ]),
                'num_days' => $this->dateFormatter->diffInDate([
                    $request->post('start_date'),
                    $request->post('end_date')
                ]),
            ]);


            EmployeeOB::find($id)->update($request->except('file_attachment'));

            DB::commit();

            $notification = array(
                'message' => 'OB Updated Successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);

        } catch (\Exception $e){
            DB::rollBack();

            dd($e);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeOB  $employeeOB
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        EmployeeOB::destroy($id);

        $notification = array(
            'message' => 'OB Deleted Successfully',
            'alert-type' => 'warning'
        );
        return redirect()->back()->with($notification);
    }


    public function obApproval(){

        $data['obs'] = EmployeeOB::all()->where('status', 'Pending');

        return view('Timesheet.OB.ob-approval', $data);

    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(Request $request){

        Excel::import(new OBImports, $request->file('import_file'));


        $notification = array(
            'message' => 'Bulk Upload Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
}
