<?php

namespace App\Http\Controllers;

use App\Billing;
use App\BillingHeader;
use App\Companies;
use App\Services\AttendanceService;
use App\Services\BillingService;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BillingController extends Controller
{

    /**
     * @var EmployeeService
     */
    private $employeeService;
    /**
     * @var AttendanceService
     */
    private $billingService;


    /**
     * AttendanceRecordController constructor.
     * @param  EmployeeService  $employeeService
     * @param  BillingService  $billingService
     */
    public function __construct(EmployeeService $employeeService, BillingService $billingService)
    {
        $this->employeeService = $employeeService;
        $this->billingService = $billingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['companies'] = Companies::all();
        $data['billing_headers'] = BillingHeader::all();

        return view('Payroll.Billing.billing-run', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $from = $request->post('date_from');
        $to = $request->post('date_to');
        $company_id = $request->post('company_id');


        //create billing header
        $billingHeader = BillingHeader::create([
            'company_id' => $request->post('company_id'),
            'date_from' => $request->post('date_from'),
            'date_to' => $request->post('date_to'),
            'description' => $request->post('description'),
            'status' => 'Pending',
            'created_by' => auth()->user()->id
         ]);

        //get all employees from company
        $company_employees = $this->employeeService->getEmployeesByCompany($company_id);


        //process all attendance record
        $computed_hours = $this->billingService->computeBilling($billingHeader->id, $company_employees, $from, $to, $company_id);



        $notification = array(
            'message' => 'Billing Processed and Saved Successfully',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function show(Billing $billing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function edit(Billing $billing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $total_labor = $request->post('total_labor');

            $total = $total_labor + $request->post('sil') + $request->post('th_month') + $request->post('separation') + $request->post('ppe');
            $grand_total = $total + $request->post('admin');

            $request->merge([
                'total' => $total,
                'grand_total' => $grand_total
            ]);


            Billing::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Billing Info Updated Successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            Billing::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Billing Info Deleted',
                'alert-type' => 'Success',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    public function billingList($id)
    {

        $data['billings'] = Billing::all()->where('billing_header', $id);


        return view('Payroll.Billing.billing-list', $data);
    }
}
