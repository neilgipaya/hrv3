<?php

namespace App\Http\Controllers;

use App\Companies;
use App\CostCenter;
use App\Departments;
use App\Http\Requests\EmployeeRequest;
use App\Paygroups;
use App\ReportMethod;
use App\Services\EmployeeService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class EmployeesController extends Controller
{
    /**
     * @var EmployeeService
     */
    private $employeeService;

    /**
     * EmployeesController constructor.
     * @param  EmployeeService  $employeeService
     */
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['employees'] = User::all()->sortByDesc('id');

        return view('Employees.employees-list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $data['pay_groups'] = Paygroups::all();
        $data['companies'] = Companies::all();
        $data['roles'] = Role::all();

        return view('Employees.employees-create', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EmployeeRequest  $request
     * @return void
     */
    public function store(EmployeeRequest $request)
    {
        //

        $createEmployee = $this->employeeService->make($request);

        if ($createEmployee){

            return response()->json([
               'message' => 'Employee Created Successfully',
               'type' => 'success'
            ], 200);

        }

            return response()->json([
               'message' => 'Unable to create Employee'
            ], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $data['employee'] = User::find($id);
        $data['companies'] = Companies::all();
        $data['pay_groups'] = Paygroups::all();
        $data['employees'] = User::all()->sortBy('lastname');
        $data['approvers'] = ReportMethod::all()->where('users_id', $id);
        $data['roles'] = Role::all();



        return view('Employees.employee-profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            if ($request->has('role')){
                $user = User::find($id);
                $user->syncRoles($request->post('role'));
            }

            if ($request->post('password') != null) {
                User::find($id)->update($request->all());
            } else {
                User::find($id)->update($request->except('password'));
            }



            DB::commit();
            $notification = array(
                'message' => 'Information updated successfully',
                'alert-type' => 'success',
            );


            return redirect()->back()->with($notification);
         } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getEmployees(){

        $employees = User::all()->sortByDesc('id');

        return DataTables::of($employees)
            ->addColumn('department', function ($row){
                $costCenter = $row->jobDetails->costCenter->cost_center_name;
                $html = "<span>". $costCenter . "</span>";
                return $html;
            })
            ->make(true);

    }
}
