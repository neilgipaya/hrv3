<?php

namespace App\Http\Controllers;

use App\EmployeeLeaves;
use App\EmployeeOvertime;
use App\Http\Controllers\Formatter\DateFormatter;
use App\Http\Controllers\Helper\TimesheetHelper;
use App\Imports\LeaveImports;
use App\Imports\OvertimeImport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeOvertimeController extends Controller
{
    /**
     * @var TimesheetHelper
     */
    private $timesheetHelper;
    /**
     * @var DateFormatter
     */
    private $dateFormatter;

    /**
     * EmployeeOvertimeController constructor.
     * @param  TimesheetHelper  $timesheetHelper
     * @param  DateFormatter  $dateFormatter
     */
    public function __construct(TimesheetHelper $timesheetHelper, DateFormatter $dateFormatter)
    {
        $this->timesheetHelper = $timesheetHelper;
        $this->dateFormatter = $dateFormatter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['overtimes'] = EmployeeOvertime::all()->where('users_id', auth()->user()->id);

        return view('Timesheet.Overtime.employee-overtime', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $approvers = $this->timesheetHelper->checkApprovers();

        if (!isset($approvers) || empty($approvers)){

            return redirect()->back()->with(['message' => 'You currently dont have any approver', 'type' => 'error']);
        }

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'total_hours' => $this->dateFormatter->diffInHours([
                    $request->post('time_in'),
                    $request->post('time_out')
                ]),
                'users_id' => auth()->user()->id,
                'status' => 'Pending',
                'approver_id' => $approvers,
                'filling_date' => Carbon::today()->format('Y-m-d')
            ]);

            EmployeeOvertime::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Overtime Created Successfully',
                'alert-type' => 'success',
            );

            return redirect()->back()->with($notification);
         } catch (\Exception $e) {

            dd($e);
            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeOvertime  $employeeOvertime
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeOvertime $employeeOvertime)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeOvertime  $employeeOvertime
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeOvertime $employeeOvertime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            $request->merge([
                'total_hours' => $this->dateFormatter->diffInHours([
                    $request->post('time_in'),
                    $request->post('time_out')
                ])
            ]);

            EmployeeOvertime::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Overtime Updated Successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //


            EmployeeOvertime::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Overtime Deleted Successfully',
                'alert-type' => 'warning',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    public function overtimeApproval()
    {


        $data['overtimes'] = EmployeeOvertime::all()->where('status', 'Pending');

        return view('Timesheet.Overtime.overtime-approval', $data);

    }

    public function import(Request $request){

        Excel::import(new OvertimeImport, $request->file('import_file'));


        $notification = array(
            'message' => 'Bulk Upload Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
}
