<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //


    public function index(){

        if (auth()->user()->can('admin-dashboard')){
            return view('Dashboard.index-admin');
        }

        return redirect()->to('my-dashboard');

    }

    public function indexEmployee(){

        return view('Dashboard.index-member');
    }
}
