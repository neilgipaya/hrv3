<?php

namespace App\Http\Controllers;

use App\Paygroups;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaygroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['pay_groups'] = Paygroups::all();

        return view('Organization.pay-groups', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            Paygroups::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Pay Groups Created Successfully',
                'alert-type' => 'success',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paygroups  $paygroups
     * @return \Illuminate\Http\Response
     */
    public function show(Paygroups $paygroups)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paygroups  $paygroups
     * @return \Illuminate\Http\Response
     */
    public function edit(Paygroups $paygroups)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            Paygroups::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Pay Groups updated successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            Paygroups::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Pay groups deleted successfully',
                'alert-type' => 'warning',
            );

            return redirect()->back()->with($notification);
         } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }
}
