<?php

namespace App\Http\Controllers;

use App\DTRProcessed;
use Illuminate\Http\Request;

class DTRProcessedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DTRProcessed  $dTRProcessed
     * @return \Illuminate\Http\Response
     */
    public function show(DTRProcessed $dTRProcessed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DTRProcessed  $dTRProcessed
     * @return \Illuminate\Http\Response
     */
    public function edit(DTRProcessed $dTRProcessed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DTRProcessed  $dTRProcessed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DTRProcessed $dTRProcessed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DTRProcessed  $dTRProcessed
     * @return \Illuminate\Http\Response
     */
    public function destroy(DTRProcessed $dTRProcessed)
    {
        //
    }
}
