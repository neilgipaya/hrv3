<?php

namespace App\Http\Controllers;

use App\EmploymentInformation;
use Illuminate\Http\Request;

class EmploymentInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmploymentInformation  $employmentInformation
     * @return \Illuminate\Http\Response
     */
    public function show(EmploymentInformation $employmentInformation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmploymentInformation  $employmentInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(EmploymentInformation $employmentInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmploymentInformation  $employmentInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmploymentInformation $employmentInformation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmploymentInformation  $employmentInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmploymentInformation $employmentInformation)
    {
        //
    }
}
