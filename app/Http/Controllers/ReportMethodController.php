<?php

namespace App\Http\Controllers;

use App\ReportMethod;
use Facade\FlareClient\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {

            ReportMethod::create($request->all());
            DB::commit();
            $notification = array(
                'message' => 'Reporting Method Created Successfully',
                'alert-type' => 'Success'
            );


            return redirect()->back()->with($notification);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReportMethod  $reportMethod
     * @return \Illuminate\Http\Response
     */
    public function show(ReportMethod $reportMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReportMethod  $reportMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportMethod $reportMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportMethod  $reportMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportMethod $reportMethod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReportMethod  $reportMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportMethod $reportMethod)
    {
        //
    }
}
