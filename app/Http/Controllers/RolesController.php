<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    //

    public function index(){

        $role = Role::create([
            'name' => 'Employee',
            'guard_name' => 'web'
        ]);

        $permissions = [
            'employee-dashboard',
            'employee-profile',
            'employee-report-to',
            'employee-view-salary',
            'employee-salary-details',
            'employee-job-details',
            'employee-personal-info',
            'core-hr',
            'awards-list',
            'transfer-list',
            'resignation-list',
            'resignation-create',
            'resignation-edit',
            'resignation-delete',
            'complaints-list',
            'complaints-create',
            'complaints-edit',
            'complaints-delete',
            'warnings-list',
            'warnings-create',
            'warnings-edit',
            'warnings-delete',
            'organization',
            'announcements-list',

            'policy-list',

            'timesheet',

            'attendance-list',

            'monthly-timesheet',

            'holidays',
            'leaves',
            'overtime',
            'official-business',

            'payroll',

            'payslip',

        ];

        $role->syncPermissions($permissions);


    }
}
