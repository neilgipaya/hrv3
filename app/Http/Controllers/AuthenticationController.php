<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    //

    public function login(LoginRequest $request)
    {

        try{

            $credentials = $request->only('username', 'password');
            $remember = $request->post('remember');

            if (Auth::attempt($credentials, $remember)){

                if (Auth::user()->enable_login == "Yes"){

                    return response()->json([
                        'message' => 'Login successfully',
                        'success' => true
                    ], 200);

                } else {

                    return response()->json([
                        'message' => 'Login unauthorized',
                        'success' => false
                    ], 201);

                }

            }

            return response()->json([
                'message' => 'Invalid Username / Password',
                'success' => false
            ], 200);

        } catch (\Exception $exception){
            return response()->json([
                'message' => $exception->getMessage(),
            ], 500);

        }
    }
}
