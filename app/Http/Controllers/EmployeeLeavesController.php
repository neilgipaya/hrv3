<?php

namespace App\Http\Controllers;

use App\EmployeeLeaves;
use App\EmploymentInformation;
use App\Http\Controllers\Formatter\DateFormatter;
use App\Http\Controllers\Formatter\StringFormatter;
use App\Http\Controllers\Helper\TimesheetHelper;
use App\Imports\DTRImports;
use App\Imports\LeaveImports;
use App\Services\LeaveService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use function foo\func;

class EmployeeLeavesController extends Controller
{
    /**
     * @var LeaveService
     */
    private $leaveService;
    /**
     * @var StringFormatter
     */
    private $formatter;
    /**
     * @var DateFormatter
     */
    private $dateFormatter;
    /**
     * @var TimesheetHelper
     */
    private $timesheetHelper;

    /**
     * EmployeeLeavesController constructor.
     * @param  LeaveService  $leaveService
     * @param  StringFormatter  $formatter
     * @param  DateFormatter  $dateFormatter
     * @param  TimesheetHelper  $timesheetHelper
     */
    public function __construct(LeaveService $leaveService, StringFormatter $formatter, DateFormatter $dateFormatter, TimesheetHelper $timesheetHelper)
    {
        $this->leaveService = $leaveService;
        $this->formatter = $formatter;
        $this->dateFormatter = $dateFormatter;
        $this->timesheetHelper = $timesheetHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['leaves'] = EmployeeLeaves::all()->where('users_id', \auth()->user()->id);
        return view('Timesheet.Leave.employee-leaves', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $approvers = $this->timesheetHelper->checkApprovers();

        if (!isset($approvers) || empty($approvers)){

            return response()->json(['message' => 'You currently dont have any approver', 'type' => 'error']);
        }

        $request->merge([
            'users_id' => \auth()->user()->id,

            'approver_id' => $approvers,

            'leave_amount' => $request->post('leave_schedule') == 0 ? $this->dateFormatter->diffInDate(array(
                $request->post('from_date'),
                $request->post('to_date')
            )) : 0.5,

            'status' => 'Pending',

            'filling_date' => Carbon::today()->format('Y-m-d'),

        ]);

        $request->merge([
            'total_hours' => $request->post('leave_schedule') == 0 ? $request->post('leave_amount') * 8 : 4,
            'leave_without_pay' => $request->post('leave_without_pay')
        ]);

        $leave = $this->leaveService->processLeave($request);

        if ($request->has('update_leave')){
            return redirect()->back()->with($leave);
        }
        return $leave;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeLeaves  $employeeLeaves
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeLeaves $employeeLeaves)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeLeaves  $employeeLeaves
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeLeaves $employeeLeaves)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        if ($request->has('approval')){

            EmployeeLeaves::find($id)->update($request->all());

            $notification = array(
                'message' => 'Leave Approved!',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);


        }

        if ($request->has('decline')){

            EmployeeLeaves::find($id)->update($request->all());

            $notification = array(
                'message' => 'Leave Declined!',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);


        }



        $request->merge([
            'users_id' => \auth()->user()->id,
            'leave_amount' => $request->post('leave_schedule') == 0 ? $this->dateFormatter->diffInDate(array(
                $request->post('from_date'),
                $request->post('to_date')
            )) : 0.5,
            'status' => 'Pending',
            'filling_date' => Carbon::today()->format('Y-m-d'),
        ]);

        $request->merge([
            'total_hours' => $request->post('leave_schedule') == 0 ? $request->post('leave_amount') * 8 : 4,
            'leave_without_pay' => $request->post('leave_without_pay')
        ]);

        $leave = $this->leaveService->processLeave($request->except('_token', '_method'));

        return $leave;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            EmployeeLeaves::destroy($id);
            DB::commit();
            $notification = array(
                'message' => 'Employee Leave Removed Successfully',
                'alert-type' => 'warning'
            );


           return redirect()->back()->with($notification);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function leaveApproval(){

        $data['leaves'] = EmployeeLeaves::all()->where('status', 'Pending');

        return view('Timesheet.Leave.leave-approval', $data);

    }

    public function leaveTable(Request $request){

//        if ($request->ajax()){

            $leave = EmployeeLeaves::all()->where('users_id', \auth()->user()->id);

            return DataTables::of($leave)
                ->addColumn('employee', function ($row){
                    $employee = $row->users->lastname . " " . $row->users->firstname;
                    return $employee;
                })
                ->addColumn('company', function ($row){
                    $company = $row->users->jobDetails->company->company_name;
                    return $company;
                })
                ->addColumn('leave_date', function($row){
                    $from = Carbon::parse($row->from_date)->format('F d, Y');
                    $to = Carbon::parse($row->to_date)->format('F d, Y');

                    if ($from === $to){
                        return $from;
                    }

                    return $from . " - " . $to;
                })
                ->addColumn('action', function (){
                    return null;
                })
                ->addIndexColumn()
                ->make(true);

//        }

    }

    public function leaveBalance($leaveType)
    {

        $leave_balance = EmploymentInformation::where('users_id', Auth::user()->id)->pluck($leaveType)->first();

        return response()->json(['balance' => $leave_balance], 200);
    }

    public function import(Request $request){

        Excel::import(new LeaveImports, $request->file('import_file'));


        $notification = array(
            'message' => 'Bulk Upload Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
}
