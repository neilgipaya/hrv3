<?php

namespace App\Http\Controllers;

use App\PayrollHeader;
use App\PayrollRuns;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PayrollHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['payrolls'] = PayrollHeader::all();

        return view('Payroll.payroll-list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        //

        $data['payrolls'] = DB::table('users')
            ->join('payroll_runs', 'payroll_runs.user_id', '=', 'users.id')
            ->where('payroll_runs.payroll_header_id', $id)
            ->orderBy('users.lastname', 'ASC')
            ->get();


        return view('Payroll.payroll-run-list', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayrollHeader  $payrollHeader
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollHeader $payrollHeader)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayrollHeader  $payrollHeader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayrollHeader $payrollHeader)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayrollHeader  $payrollHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollHeader $payrollHeader)
    {
        //
    }
}
