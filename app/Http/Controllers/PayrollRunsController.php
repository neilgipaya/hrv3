<?php

namespace App\Http\Controllers;

use App\BillingHeader;
use App\Companies;
use App\Paygroups;
use App\PayrollHeader;
use App\PayrollRuns;
use App\Services\PayrollRunService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PayrollRunsController extends Controller
{
    /**
     * @var PayrollRunService
     */
    private $payrollRunService;

    /**
     * PayrollRunsController constructor.
     * @param  PayrollRunService  $payrollRunService
     */
    public function __construct(PayrollRunService $payrollRunService)
    {

        $this->payrollRunService = $payrollRunService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['employees'] = User::all();
        $data['pay_groups'] = Paygroups::all();
        $data['companies'] = Companies::all();
        $data['billings'] = BillingHeader::all()->where('status', 'Approved');

        return view('Payroll.payroll-run', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $from = $request->post('from');
        $to = $request->post('to');


        //set the payroll options
        $options = [
            'compute_sss' => $request->post('compute_sss'),
            'compute_pagibig' => $request->post('compute_pagibig'),
            'compute_philhealth' => $request->post('compute_philhealth'),
            'compute_tax' => $request->post('compute_tax'),
            'compute_loan' => $request->post('compute_loan'),
        ];

        try{

            //check if by company or bu employee

            if (empty($request->post('company'))){

                $payrollHeader = PayrollHeader::create([
                    'from' => $from,
                    'to' => $to,
                    'payroll_run_type' => $request->post('run_type'),
                    'period' => $request->post('period'),
                    'payroll_by' => Auth::user()->id,
                    'compute_sss' => $request->post('compute_sss'),
                    'compute_pagibig' => $request->post('compute_pagibig'),
                    'compute_philhealth' => $request->post('compute_philhealth'),
                    'compute_tax' => $request->post('compute_tax'),
                    'compute_loan' => $request->post('compute_loan'),
                    'description' => $request->post('description')
                ]);

                $header_id = $payrollHeader->id;

                $computedPayroll = $this->payrollRunService->calculateEmployeePayroll($request->post('employees'), $from, $to, $options, $header_id);

                $notification = array(
                    'message' => 'Payroll Processed and Saved Successfully',
                    'alert-type' => 'success',
                );

                return redirect()->back()->with($notification);

            }

        } catch (\Exception $exception){

            dd($exception);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayrollRuns  $payrollRuns
     * @return \Illuminate\Http\Response
     */
    public function show(PayrollRuns $payrollRuns)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayrollRuns  $payrollRuns
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollRuns $payrollRuns)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayrollRuns  $payrollRuns
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayrollRuns $payrollRuns)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayrollRuns  $payrollRuns
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollRuns $payrollRuns)
    {
        //
    }
}
