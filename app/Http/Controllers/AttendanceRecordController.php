<?php

namespace App\Http\Controllers;

use App\AttendanceRecord;
use App\Companies;
use App\Services\AttendanceService;
use App\Services\EmployeeService;
use App\User;
use Illuminate\Http\Request;

class AttendanceRecordController extends Controller
{
    /**
     * @var EmployeeService
     */
    private $employeeService;
    /**
     * @var AttendanceService
     */
    private $attendanceService;


    /**
     * AttendanceRecordController constructor.
     * @param  EmployeeService  $employeeService
     * @param  AttendanceService  $attendanceService
     */
    public function __construct(EmployeeService $employeeService, AttendanceService $attendanceService)
    {
        $this->employeeService = $employeeService;
        $this->attendanceService = $attendanceService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['companies'] = Companies::all();

        return view('Payroll.DTR.process-attendance', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $from = $request->post('date_from');
        $to = $request->post('date_to');
        $company_id = $request->post('company_id');


        //get all employees from company
        $company_employees = $this->employeeService->getEmployeesByCompany($company_id);


        //process all attendance record
        $computed_hours = $this->attendanceService->computeHours($company_employees, $from, $to);



        $notification = array(
            'message' => 'DTR Processed and Saved Successfully, Please proceed on Payroll',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttendanceRecord  $attendanceRecord
     * @return \Illuminate\Http\Response
     */
    public function show(AttendanceRecord $attendanceRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttendanceRecord  $attendanceRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(AttendanceRecord $attendanceRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttendanceRecord  $attendanceRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttendanceRecord $attendanceRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttendanceRecord  $attendanceRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttendanceRecord $attendanceRecord)
    {
        //
    }
}
