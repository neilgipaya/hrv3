<?php


namespace App\Http\Controllers\Formatter;


use Carbon\Carbon;

class StringFormatter
{
    /**
     * @param $word
     * @return string
     */
    public function explodeTags($tags){
        $str = "";
        foreach ($tags as $tag){
            $str .= $tag . ",";
        }

        return substr($str, 0, -1);
    }

    /**
     * @param $string
     * @return array
     */
    public function explodeComma($string){

        $separated = explode(',', $string);
        return $separated;
    }

    public function cleanComma($string){

        return str_replace(",", "", $string);

    }

}
