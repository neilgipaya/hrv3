<?php


namespace App\Http\Controllers\Formatter;


use Carbon\Carbon;

class DateFormatter
{
    public function diffInDate($param){


        $date = Carbon::parse($param[0]);
        $diff = $date->diffInDays($param[1]) + 1;

        if(!empty($diff)){
            return $diff;
        }
        return [];
    }

    public function diffInHours($param){

        $date = Carbon::parse($param[0]);
        $diff = $date->diffInHours($param[1]) + 1;

        if(!empty($diff)){
            return $diff;
        }
        return [];
    }

    public function DateTimeToHours($timestamp)
    {

        $format = Carbon::parse($timestamp);

        return $format->format('h:i A');

    }
}
