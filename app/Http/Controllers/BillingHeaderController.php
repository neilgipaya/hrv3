<?php

namespace App\Http\Controllers;

use App\Billing;
use App\BillingHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BillingHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BillingHeader  $billingHeader
     * @return \Illuminate\Http\Response
     */
    public function show(BillingHeader $billingHeader)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BillingHeader  $billingHeader
     * @return \Illuminate\Http\Response
     */
    public function edit(BillingHeader $billingHeader)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BillingHeader  $billingHeader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            BillingHeader::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Billing Updated Successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BillingHeader  $billingHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            BillingHeader::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Billing Deleted Successfully',
                'alert-type' => 'warning',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }
    }
}
