<?php


namespace App\Http\Controllers\Helper;


use App\ReportMethod;
use Illuminate\Support\Facades\Auth;

class TimesheetHelper
{

    public function checkApprovers(){

        $approvers = ReportMethod::all()->where('users_id', Auth::user()->id)->where('reporting_type', 'Direct');

        if (isset($approvers)){
            return $this->formatApprover($approvers);
        }

        return null;
    }



    private function formatApprover($approvers){

        $str = "";
        foreach ($approvers as $approver){
            $str .= $approver->approver_id . ",";
        }

        return substr($str, 0, -1);
    }
}
