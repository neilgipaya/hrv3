<?php

namespace App\Http\Controllers;

use App\OTRates;
use Illuminate\Http\Request;

class OTRatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OTRates  $oTRates
     * @return \Illuminate\Http\Response
     */
    public function show(OTRates $oTRates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OTRates  $oTRates
     * @return \Illuminate\Http\Response
     */
    public function edit(OTRates $oTRates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OTRates  $oTRates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OTRates $oTRates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OTRates  $oTRates
     * @return \Illuminate\Http\Response
     */
    public function destroy(OTRates $oTRates)
    {
        //
    }
}
