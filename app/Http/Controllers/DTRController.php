<?php

namespace App\Http\Controllers;

use App\AttendanceTime;
use App\Companies;
use App\DTR;
use App\Imports\DTRImports;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DTRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['attendance'] = DTR::whereDate('attendance_date', Carbon::today())->get();

        return view('Timesheet.Attendance.attendance', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DTR  $dTR
     * @return \Illuminate\Http\Response
     */
    public function show(DTR $dTR)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DTR  $dTR
     * @return \Illuminate\Http\Response
     */
    public function edit(DTR $dTR)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DTR  $dTR
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DTR $dTR)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DTR  $dTR
     * @return \Illuminate\Http\Response
     */
    public function destroy(DTR $dTR)
    {
        //
    }

    public function monthlyTimesheet(){

        $today = Carbon::now();
        $start = $today->startOfMonth();
        $end = $today->endOfMonth();

        $period = CarbonPeriod::create(Carbon::parse($start), Carbon::parse($end));

        dd($period);

    }

    public function attendanceUpdate(Request $request){

        if (empty($request->get('date_from'))){
            $filter_from = Carbon::now()->startOfMonth();
            $filter_to = Carbon::now()->endOfMonth();
        } else {
            $filter_from = $request->get('date_from');
            $filter_to = $request->get('date_to');
        }

        $data['companies'] = Companies::all();

        $data['attendances'] = AttendanceTime::all()->sortByDesc('id')->where('attendance_date', '>=', $filter_from)->where('attendance_date', '<=', $filter_to);

        return view('Timesheet.Attendance.attendance-update', $data);

    }

    public function attendanceSave(Request $request, $id)
    {

        DB::beginTransaction();

        try {
            //

            AttendanceTime::find($id)->update($request->except('update'));

            DB::commit();
            $notification = array(
                'message' => 'Attendance Updated Successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
         } catch (\Exception $e) {

            DB::rollback();
            dd($e);
            $notification = array(
                'message' => 'There is a problem with your action. Please contact your administrator',
                'alert-type' => 'error',
            );

            return redirect()->back()->with($notification);
        }

    }

    public function importAttendance(){

        return view('Timesheet.Attendance.import-attendance');
    }

    public function processImport(Request $request){

        Excel::import(new DTRImports, $request->file('import_file'));


        $notification = array(
            'message' => 'Bulk Upload Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
}
