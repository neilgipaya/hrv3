<?php

namespace App\Http\Controllers;

use App\Companies;
use App\CostCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['costCenter'] = CostCenter::all();
        $data['companies'] = Companies::all();

        return view('Organization.cost-centers', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        DB::beginTransaction();

        try {
            //

            CostCenter::create($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Cost center created successfully',
                'alert-type' => 'success',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function show(CostCenter $costCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(CostCenter $costCenter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();

        try {
            //

            CostCenter::find($id)->update($request->all());

            DB::commit();
            $notification = array(
                'message' => 'Cost center updated successfully',
                'alert-type' => 'info',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();

        try {
            //

            CostCenter::destroy($id);

            DB::commit();
            $notification = array(
                'message' => 'Cost Center deleted successfully',
                'alert-type' => 'warning',
            );

            return redirect()->back()->with($notification);
        } catch (\Exception $e) {

            DB::rollback();
            dd($e->getMessage());
        }
    }


    /**
     * Get cost center
     * @param $company
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchCostCenter($company){

        $data = CostCenter::all()->where('companies_id', $company);

        return response()->json($data);

    }
}
