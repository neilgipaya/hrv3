<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companies extends Model
{
    //

    use SoftDeletes;

    protected $guarded = [];


    public function department(){

        return $this->hasOne('App\Departments');
    }

    public function announcement(){

        return $this->hasOne('App\Announcements');
    }

    public function jobDetails(){

        return $this->hasOne('App\EmployeeJobDetails');

    }

    public function billingHeader()
    {

        return $this->hasOne('App\BillingHeader');

    }
}
