<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OTRates extends Model
{
    //

    protected $table = "ot_rates";

    protected $guarded = [];

}
