<?php


namespace App\Providers;


use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use function Matrix\trace;

class BladeServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return string
     */
    public function boot()
    {
        //

        Blade::directive('checkApprover', function ($string){
            $approver_check = explode(',', $string);

            if (in_array(auth()->user()->id, $approver_check)){
                return true;
            }

            return false;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
