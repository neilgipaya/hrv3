<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $permissions = [
            'admin-dashboard',
            'employee-dashboard',
            'employee-create',
            'employee-list',
            'employee-import',
            'employee-edit',
            'employee-delete',
            'employee-profile',
            'employee-report-to',
            'employee-view-salary',
            'employee-salary-details',
            'employee-job-details',
            'employee-personal-info',
            'core-hr',
            'awards-list',
            'awards-create',
            'awards-edit',
            'awards-delete',
            'transfer-list',
            'transfer-create',
            'transfer-edit',
            'transfer-delete',
            'resignation-list',
            'resignation-create',
            'resignation-edit',
            'resignation-delete',
            'complaints-list',
            'complaints-create',
            'complaints-edit',
            'complaints-delete',
            'warnings-list',
            'warnings-create',
            'warnings-edit',
            'warnings-delete',
            'organization',
            'company-list',
            'company-create',
            'company-edit',
            'company-delete',
            'cost-center-list',
            'cost-center-create',
            'cost-center-edit',
            'cost-center-delete',
            'departments-list',
            'departments-create',
            'departments-edit',
            'departments-delete',
            'pay-groups-list',
            'pay-groups-create',
            'pay-groups-edit',
            'pay-groups-delete',
            'announcements-list',
            'announcements-create',
            'announcements-edit',
            'announcements-delete',
            'policy-list',
            'policy-create',
            'policy-edit',
            'policy-delete',
            'timesheet',
            'attendance-list',
            'monthly-timesheet',
            'attendance-update',
            'import-attendance',
            'office-shifts',
            'holidays',
            'leaves',
            'overtime',
            'official-business',
            'payroll',
            'process-dtr',
            'billing',
            'process-payroll',
            'payroll-list',
            'payslip',
            'overtime-rates',
            'reports',
        ];

        foreach ($permissions as $permission){
            \Spatie\Permission\Models\Permission::create([
                'name' => $permission,
                'guard_name' => 'web'
            ]);
        }
    }
}
