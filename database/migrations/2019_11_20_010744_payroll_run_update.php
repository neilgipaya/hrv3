<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PayrollRunUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payroll_runs', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->after('id');

            //payroll_info
            $table->double('monthly_salary')->default(0);
            $table->double('daily_salary')->default(0);
            $table->double('hourly_rate')->default(0);


            //regular hours
            $table->double('basic_hours')->default(0);
            $table->double('rd_hours')->default(0);
            $table->double('sh_hours')->default(0);
            $table->double('lh_hours')->default(0);
            $table->double('sh_rd_hours')->default(0);
            $table->double('lh_rd_hours')->default(0);
            $table->double('dh_hours')->default(0);
            $table->double('dh_rd_hours')->default(0);

            //ot hours
            $table->double('overtime')->default(0);
            $table->double('rd_ot_hours')->default(0);
            $table->double('sh_ot_hours')->default(0);
            $table->double('lh_ot_hours')->default(0);
            $table->double('sh_rd_ot_hours')->default(0);
            $table->double('lh_rd_ot_hours')->default(0);
            $table->double('dh_ot_hours')->default(0);
            $table->double('dh_rd_ot_hours')->default(0);


            $table->double('late')->default(0);
            $table->double('undertime')->default(0);
            $table->double('night_diff')->default(0);
            $table->double('night_diff_ot')->default(0);

            //payroll part
            $table->double('total_gross')->default(0);
            $table->double('total_company_loan')->default(0);
            $table->double('total_pagibig_loan')->default(0);
            $table->double('total_sss_loan')->default(0);
            $table->double('sss_deduction')->default(0);
            $table->double('pagibig_deduction')->default(0);
            $table->double('philhealth_deduction')->default(0);
            $table->double('witholding_tax')->default(0);
            $table->double('total_net_pay')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
