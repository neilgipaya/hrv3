<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id');
            $table->string('lastname');
            $table->string('firstname');
            $table->string('middlename');
            $table->enum('gender', array('Male', 'Female'));
            $table->date('date_of_birth');
            $table->string('civil_status');
            $table->string('contact_no');
            $table->string('home_address');
            $table->string('zip_code');
            $table->string('username');
            $table->enum('enable_login', array('Yes', 'No'));
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
