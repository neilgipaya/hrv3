<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('from');
            $table->date('to');
            $table->string('description');
            $table->string('compute_sss');
            $table->string('compute_pagibig');
            $table->string('compute_philhealth');
            $table->string('compute_tax');
            $table->string('compute_loan');
            $table->string('payroll_run_type');
            $table->string('period');
            $table->unsignedInteger('payroll_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_headers');
    }
}
