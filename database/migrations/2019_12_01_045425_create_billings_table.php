<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('company_id');
            $table->date('date_from');
            $table->date('date_to');
            //regular hours

            $table->unsignedBigInteger('daily_rate')->default(0);
            $table->unsignedBigInteger('hourly_rate')->default(0);

            $table->unsignedBigInteger('rh')->default(0);
            $table->unsignedBigInteger('php_rh')->default(0);

            $table->unsignedBigInteger('late')->default(0);
            $table->unsignedBigInteger('php_late')->default(0);

            $table->unsignedBigInteger('php_absent')->default(0);
            $table->unsignedBigInteger('absent')->default(0);

            $table->unsignedBigInteger('undertime')->default(0);
            $table->unsignedBigInteger('php_undertime')->default(0);

            $table->unsignedBigInteger('basic_pay')->default(0);

            $table->unsignedBigInteger('nsd');
            $table->unsignedBigInteger('php_nsd');

            $table->unsignedBigInteger('nsd2');
            $table->unsignedBigInteger('php_nsd2');

            $table->unsignedBigInteger('rot')->default(0);
            $table->unsignedBigInteger('php_rot')->default(0);

            $table->unsignedBigInteger('rdot')->default(0);
            $table->unsignedBigInteger('php_rdot')->default(0);

            $table->unsignedBigInteger('shot')->default(0);
            $table->unsignedBigInteger('php_shot')->default(0);

            $table->unsignedBigInteger('shote')->default(0);
            $table->unsignedBigInteger('php_shote')->default(0);

            $table->unsignedBigInteger('shrd')->default(0);
            $table->unsignedBigInteger('php_shrd')->default(0);

            $table->unsignedBigInteger('shrde')->default(0);
            $table->unsignedBigInteger('php_shrde')->default(0);

            $table->unsignedBigInteger('lhot')->default(0);
            $table->unsignedBigInteger('php_lhot')->default(0);

            $table->unsignedBigInteger('lhote')->default(0);
            $table->unsignedBigInteger('php_lhote')->default(0);

            $table->unsignedBigInteger('lhrd')->default(0);
            $table->unsignedBigInteger('php_lhrd')->default(0);

            $table->unsignedBigInteger('lhrde')->default(0);
            $table->unsignedBigInteger('php_lhrde')->default(0);

            $table->unsignedBigInteger('holiday')->default(0);
            $table->unsignedBigInteger('php_holiday')->default(0);

            $table->unsignedBigInteger('php_total_ot')->default(0);
            $table->unsignedBigInteger('cola')->default(0);
            $table->unsignedBigInteger('gross_pay')->default(0);

            $table->unsignedBigInteger('total_basic_pay')->default(0);

            $table->unsignedBigInteger('er_sss')->default(0);
            $table->unsignedBigInteger('er_ec')->default(0);
            $table->unsignedBigInteger('er_hdmf')->default(0);
            $table->unsignedBigInteger('er_phic')->default(0);
            $table->unsignedBigInteger('allowance')->default(0);
            $table->unsignedBigInteger('total_labor')->default(0);
            $table->unsignedBigInteger('sil')->default(0);
            $table->unsignedBigInteger('13th_month')->default(0);
            $table->unsignedBigInteger('separation')->default(0);
            $table->unsignedBigInteger('ppe')->default(0);
            $table->unsignedBigInteger('total')->default(0);
            $table->unsignedBigInteger('admin')->default(0);
            $table->unsignedBigInteger('grand_total')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
