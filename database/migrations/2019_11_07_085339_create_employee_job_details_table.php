<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_job_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->string('job_title');
            $table->unsignedInteger('departments_id');
            $table->unsignedInteger('cost_centers_id');
            $table->unsignedInteger('pay_groups_id');
            $table->foreign('departments_id')->references('id')->on('departments');
            $table->foreign('cost_centers_id')->references('id')->on('cost_centers');
            $table->foreign('pay_groups_id')->references('id')->on('pay_groups');
            $table->date('hire_date');
            $table->string('employment_status');
            $table->date('employment_effectivity_date');
            $table->float('work_hours_per_day');
            $table->string('tin_number');
            $table->string('sss_number');
            $table->string('hdmf_number');
            $table->string('philhealth_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_job_details');
    }
}
