<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('leave_type_code');
            $table->unsignedInteger('approver_id')->nullable();
            $table->date('from_date');
            $table->date('to_date');
            $table->string('reason')->nullable();
            $table->string('remarks')->nullable();
            $table->enum('without_pay', array('0', '1'));
            $table->enum('half_day', array('0', '1'));
            $table->enum('is_deducted', array('0', '1'));
            $table->string('leave_amount')->nullable();
            $table->string('status');
            $table->date('filling_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leaves');
    }
}
