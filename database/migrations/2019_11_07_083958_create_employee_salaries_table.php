<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->string('tax_code');
            $table->enum('is_minimum_earner', array('Yes', 'No'));
            $table->float('cola');
            $table->float('work_days_per_year');
            $table->float('basic_salary');
            $table->float('de_minimis');
            $table->string('ot_computation_code');
            $table->string('sss_contribution');
            $table->string('philhealth_contribution');
            $table->string('hdmf_contribution');
            $table->string('additional_hdmf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salaries');
    }
}
