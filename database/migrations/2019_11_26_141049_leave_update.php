<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LeaveUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('employment_information', function(Blueprint $table){

            $table->float('maternity')->default(0);
            $table->float('paternity')->default(0);
            $table->float('birthday')->default(0);
            $table->float('service')->default(0);
            $table->float('parental')->default(0);
            $table->float('rehabilitation')->default(0);
            $table->float('study')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
