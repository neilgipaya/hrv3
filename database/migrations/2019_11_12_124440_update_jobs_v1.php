<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateJobsV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('employee_job_details', function (Blueprint $table) {
            $table->unsignedInteger('companies_id');
            $table->date('date_resign')->nullable();
            $table->string('work_email')->nullable();
        });

        Schema::table('users', function (Blueprint $table){
           $table->string('suffix')->after('lastname')->nullable();
           $table->string('nationality')->nullable();
           $table->string('religion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
