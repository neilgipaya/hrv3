<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     * process attendance records
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->date('attendance_date');
            //regular hours
            $table->double('basic_hours')->default(0);
            $table->double('rd_hours')->default(0);
            $table->double('sh_hours')->default(0);
            $table->double('lh_hours')->default(0);
            $table->double('sh_rd_hours')->default(0);
            $table->double('lh_rd_hours')->default(0);
            $table->double('dh_hours')->default(0);
            $table->double('dh_rd_hours')->default(0);

            //ot hours
            $table->double('overtime')->default(0);
            $table->double('rd_ot_hours')->default(0);
            $table->double('sh_ot_hours')->default(0);
            $table->double('lh_ot_hours')->default(0);
            $table->double('sh_rd_ot_hours')->default(0);
            $table->double('lh_rd_ot_hours')->default(0);
            $table->double('dh_ot_hours')->default(0);
            $table->double('dh_rd_ot_hours')->default(0);


            $table->double('late')->default(0);
            $table->double('undertime')->default(0);
            $table->double('night_diff')->default(0);
            $table->double('night_diff_ot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_records');
    }
}
