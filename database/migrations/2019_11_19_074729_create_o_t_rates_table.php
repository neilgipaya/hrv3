<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOTRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * TODO: MAKE ALL OT RATE PER COMPANY
     * @return void
     */
    public function up()
    {
        Schema::create('ot_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->float('regular');
            $table->float('ot');
            $table->float('nd');
            $table->float('ndot');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ot_rates');
    }
}
