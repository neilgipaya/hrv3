<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DTR;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(DTR::class, function (Faker $faker) {
    return [
        //
        'users_id' => $faker->numberBetween('1', '30'),
        'attendance_date' => Carbon::parse($faker->dateTimeThisMonth('now'))->format('Y-m-d'),
        'time_in' => $faker->time('H:i s'),
        'time_out' => $faker->time('H:i s'),
        'late' =>  $faker->time('H:i s'),
        'undertime' =>  $faker->time('H:i s'),
        'overtime' => $faker->time('H:i s'),
        'total_work_hours' =>  $faker->time('H:i s'),
    ];
});
