<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Maatwebsite\Excel\Concerns\FromCollection;

Route::get('/force-logout', function (){
    Auth::logout();
    return redirect('/');
});

Route::group(['middleware'=>'guest'], function () {

    Route::get('/', function () {
        return view('login');
    })->name('login');


    Route::post('authenticate', 'AuthenticationController@login');

});

Route::group(['middleware'=>'auth'], function () {



    Route::resource('dashboard', 'DashboardController');
    Route::get('my-dashboard', 'DashboardController@indexEmployee');

    //TODO: ADD TO A GROUP

    Route::resource('employees', 'EmployeesController');
    Route::resource('salary', 'EmployeeSalaryController');
    Route::resource('job-details', 'EmployeeJobDetailsController');
    Route::get('employeeList', 'EmployeesController@getEmployees');
    Route::resource('reporting', 'ReportMethodController');



    Route::group(['prefix' => 'ajax'], function (){

        Route::get('fetchCostCenter/{company_id}', 'CostCenterController@fetchCostCenter');
        Route::get('fetchDepartment/{company_id}', 'DepartmentsController@fetchDepartment');
        Route::get('fetchLeaveTable', 'EmployeeLeavesController@leaveTable');
        Route::get('fetchLeaveBalance/{balance}', 'EmployeeLeavesController@leaveBalance');

    });

    Route::group(['prefix'=>'organization'], function(){

        Route::resource('companies', 'CompaniesController');
        Route::resource('costCenters', 'CostCenterController');
        Route::resource('departments', 'DepartmentsController');
        Route::resource('payGroups', 'PaygroupsController');
        Route::resource('announcements', 'AnnouncementsController');
        Route::resource('policy', 'CompanyPolicyController');

    });

    Route::group(['prefix' => 'timesheet'], function (){

        Route::resource('attendance', 'DTRController');
        Route::get('monthly-timesheet', 'DTRController@monthlyTimesheet');
        Route::get('attendance-update', 'DTRController@attendanceUpdate');
        Route::patch('attendance-save/{id}', 'DTRController@attendanceSave');
        Route::resource('leaves', 'EmployeeLeavesController');
        Route::get('leave-approval', 'EmployeeLeavesController@leaveApproval');
        Route::get('import-attendance', 'DTRController@importAttendance');
        Route::post('process-import', 'DTRController@processImport');
        Route::post('import-leave', 'EmployeeLeavesController@import');
        Route::post('import-overtime', 'EmployeeOvertimeController@import');
        Route::resource('overtime', 'EmployeeOvertimeController');
        Route::get('overtime-approval', 'EmployeeOvertimeController@overtimeApproval');
        Route::post('import-ob', 'EmployeeOBController@import');
        Route::resource('ob', 'EmployeeOBController');
        Route::get('ob-approval', 'EmployeeOBController@obApproval');

    });



    Route::group(['prefix' => 'reports'], function (){

        Route::get('attendance-report', 'ReportsController@attendanceReport');
        Route::get('generate-attendance-report', 'ReportsController@generateAttendanceReport');

    });

    Route::group(['prefix' => 'payroll'], function (){


        Route::resource('payroll-run', 'PayrollRunsController');
        Route::resource('dtr-process', 'AttendanceRecordController');
        Route::resource('payroll-list', 'PayrollHeaderController');

        Route::resource('billing', 'BillingController');
        Route::get('billing-list/{id}', 'BillingController@billingList');
        Route::resource('billing-header', 'BillingHeaderController');


    });

    Route::group(['prefix' => 'maintenance'], function(){

        Route::resource('roles', 'RolesController');

    });


    Route::get('logout', function (){

        Auth::logout();
        return redirect('/');

    });


    //DEBUG

    Route::get('faker', function (){
        $faker = Faker\Factory::create();

        $limit = 10;
        echo '<ul>';
        for ($i = 0; $i < $limit; $i++) {
            $customer = $faker->name . ' - ' . $faker->unique()->email . ' - ' . $faker->phoneNumber;
            echo "<li> $customer</li>";
        }
        echo '</ul>';
    });

});
