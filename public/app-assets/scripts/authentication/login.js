"use strict";

let login = function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let form = $("#login_form");

    let handleSubmit = function(){

        form.on("submit", function (e) {
            e.preventDefault();


            let options = {
                url: '/authenticate',
                data: form.serialize(),
                type: 'POST',
                success: function (response) {

                    if (response['success'] === true){

                        Swal.fire({

                            type: 'success',
                            title: 'Signed in successfully',
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000

                        }).then(() => {

                            window.open('/dashboard', '_self');

                        });

                    } else {

                        Swal.fire({

                            type: 'warning',
                            title: response.message,
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000

                        });

                    }

                },
                error: function (response) {
                    Swal.fire({

                        type: 'error',
                        title: response.message,
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000

                    });
                }
            };

            $.ajax(options);

        });

    };



    return {
        init: function () {

            handleSubmit();
        }
    }
}();

// Class Initialization
jQuery(document).ready(function () {

    login.init();

});
