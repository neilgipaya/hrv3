@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
                @can('import-overtime')
                <section id="basic-import">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div style="float: left">
                                        <h4 class="card-title" >Employee Overtime</h4>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <form action="/timesheet/import-overtime" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">
                                                <label>Overtime CSV</label>
                                                <input type="file" class="form-control" name="import_file" required />
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-block btn-danger">Upload Data</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @endcan
            <!--/ Zero configuration table -->

                    <section id="basic-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div style="float: left">
                                            <h4 class="card-title" >Overtime List</h4>
                                        </div>
                                        <div style="float: right;">
                                            <a href="#" data-toggle="modal" data-target="#create" class="btn btn-primary"> Create New</a>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table class="table" id="sortTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Company</th>
                                                        <th>Employee</th>
                                                        <th>Overtime</th>
                                                        <th>Overtime Date</th>
                                                        <th>Remarks</th>
                                                        <th>Total Hours</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($overtimes as $row)
                                                        <tr>
                                                            <td>{{ $row->users->jobDetails->company->company_name }}</td>
                                                            <td>{{ $row->users->lastname . " " . $row->users->firstname }}</td>
                                                            <td>{{ $row->time_in . " - " . $row->time_out }}</td>
                                                            <td>{{ $row->overtime_date }}</td>
                                                            <td>{{ $row->remarks }}</td>
                                                            <td>{{ $row->total_hours }}</td>
                                                            <td>
                                                                @if ($row->status === "Pending")
                                                                    <label class='badge badge-warning'>Pending</label>
                                                                @elseif($row->status === "Approved")
                                                                    <label class = 'badge badge-success'> Approved</label>
                                                                @else
                                                                    <label class = 'badge badge-danger'> Declined</label>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($row->status === "Pending")
                                                                <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                        data-toggle="modal" data-target="#update{{ $row->id }}">
                                                                    <i class="bx bx-pencil"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                        data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                                    <i class="bx bx-trash"></i>
                                                                </button>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Company</th>
                                                        <th>Employee</th>
                                                        <th>Overtime</th>
                                                        <th>Overtime Date</th>
                                                        <th>Remarks</th>
                                                        <th>Total Hours</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
        <!--/ Zero configuration table -->
        </div>
    </div>

    <div class="modal fade text-left" id="create" tabindex="-1"  aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <form action="/timesheet/overtime" method="POST" novalidate>
                    @csrf
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">Create Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Overtime Date</label>
                            <input type="text" class="form-control required single-daterange required" name="overtime_date" />
                        </div>
                        <div class="form-group">
                            <label>Time-In</label>
                            <input type="time" class="form-control required" name="time_in" />
                        </div>
                        <div class="form-group">
                            <label>Time-Out</label>
                            <input type="time" class="form-control required" name="time_out" />
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea class="form-control" rows="4" name="remarks" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Cancel</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Save</span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    @foreach($overtimes as $row)
    <div class="modal fade text-left" id="update{{$row->id}}" tabindex="-1"  aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <form action="/timesheet/overtime/{{ $row->id }}" method="POST" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="modal-header bg-warning">
                        <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Overtime Date</label>
                            <input type="text" class="form-control required single-daterange required" name="overtime_date" value="{{ $row->overtime_date }}" />
                        </div>
                        <div class="form-group">
                            <label>Time-In</label>
                            <input type="time" class="form-control required" name="time_in" value="{{ $row->time_in }}" />
                        </div>
                        <div class="form-group">
                            <label>Time-Out</label>
                            <input type="time" class="form-control required" name="time_out" value="{{ $row->time_out }}" />
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea class="form-control" rows="4" name="remarks" required>{{ $row->remarks }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Cancel</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Save Changes</span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    @endforeach

    @foreach($overtimes as $row)
        <div class="modal fade text-left" id="delete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/overtime/{{ $row->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    <!--primary theme Modal -->

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
