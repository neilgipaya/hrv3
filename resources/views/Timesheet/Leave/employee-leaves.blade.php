@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            @can('import-leave')
            <section id="basic-import">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Import Leave</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <form action="/timesheet/import-leave" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label>Leave CSV</label>
                                            <input type="file" class="form-control" name="import_file" required />
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-danger">Upload Data</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endcan
            <!--/ Zero configuration table -->

            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Leave List</h4>
                                </div>
                                <div style="float: right;">
                                    <a href="#" data-toggle="modal" data-target="#create" class="btn btn-primary"> Create New</a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="leaveTable">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Leave Type</th>
                                                <th>Duration</th>
                                                <th>Requested Date</th>
                                                <th>Date of Application</th>
                                                <th>Reason</th>
                                                <th>Remarks</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>id</th>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Leave Type</th>
                                                <th>Duration</th>
                                                <th>Requested Date</th>
                                                <th>Date of Application</th>
                                                <th>Reason</th>
                                                <th>Remarks</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    <!--primary theme Modal -->
    <div class="modal fade text-left" id="create" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <form action="/timesheet/leaves" id="leave_form" method="POST" novalidate>
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">Create Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Leave Type</label>
                            <select class="form-control required s2" name="leave_type_code" id="leave_type" onchange="checkLeaveCredits()" >
                                <option value=""></option>
                                <option value="sl">Sick Leave</option>
                                <option value="vl">Vacation Leave</option>
                                <option value="maternity">Maternity Leave</option>
                                <option value="paternity">Paternity Leave</option>
                                <option value="birthday">Birthday Leave</option>
                            </select>
                        </div>
                        <div class="form-group" id="parent_balance" style="display: none">
                            <label><strong>Available Leave: </strong></label>
                            <strong><span id="leave_balance"></span></strong>
                        </div>
{{--                        TODO:: CHECK ROLES AND PERMISSON IF APPLICABLE TO FILE SOMEONE WITH LEAVE  --}}
                        <div class="form-group">
                            <label>Start Date</label>
                                <input type="text" class="form-control required single-daterange required" name="from_date" />
                        </div>
                        <div class="form-group">
                            <label>End Date</label>
                            <input type="text" class="form-control required single-daterange" name="to_date" />
                        </div>
                        <div class="form-group">
                            <label>Reason</label>
                            <textarea class="form-control" rows="4" name="reason" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Without Pay?</label>
                            <select class="form-control s2" name="leave_without_pay">
                                <option value="1">Paid Leave</option>
                                <option value="0">Leave Without Pay</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Leave Schedule</label>
                            <select class="form-control s2" name="leave_schedule">
                                <option value="0">Whole Day Leave</option>
                                <option value="1">Half Day Leave</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Cancel</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Save</span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    @foreach ($leaves as $leave)
            <div class="modal fade text-left" id="update{{ $leave->id }}" aria-labelledby="myModalLabel160" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">

                        <form action="/timesheet/leaves/" method="POST" novalidate>
                            @csrf
                            <input type="hidden" name="update_leave" />
                            <input type="hidden" name="id" value="{{ $leave->id }}" />
                            <div class="modal-header bg-warning">
                                <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="bx bx-x"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Leave Type</label>
                                    <select class="form-control required s2" name="leave_type_code" id="leave_type_update" onchange="checkLeaveCreditsUpdate()" required>
                                        <option value=""></option>
                                        <option value="sl" {{ $leave->leave_type_code === "sl" ? "Selected" : "" }}>Sick Leave</option>
                                        <option value="vl" {{ $leave->leave_type_code === "vl" ? "Selected" : "" }}>Vacation Leave</option>
                                        <option value="maternity" {{ $leave->leave_type_code === "maternity" ? "Selected" : "" }}>Maternity Leave</option>
                                        <option value="paternity" {{ $leave->leave_type_code === "paternity" ? "Selected" : "" }}>Paternity Leave</option>
                                        <option value="birthday" {{ $leave->leave_type_code === "birthday" ? "Selected" : "" }}>Birthday Leave</option>
                                    </select>
                                </div>
                                <div class="form-group" id="parent_balance_update" style="display: none">
                                    <label><strong>Available Leave: </strong></label>
                                    <strong><span id="leave_balance_update"></span></strong>
                                </div>
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input type="text" class="form-control required single-daterange required" value="{{ $leave->from_date }}" name="from_date" />
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
                                    <input type="text" class="form-control required single-daterange" value="{{ $leave->to_date }}" name="to_date" />
                                </div>
                                <div class="form-group">
                                    <label>Reason</label>
                                    <textarea class="form-control" rows="4" name="reason" required>{{ $leave->reason }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Without Pay?</label>
                                    <select class="form-control s2" name="leave_without_pay">
                                        <option value="1" {{ $leave->leave_without_pay === "1" ? "Selected" : "" }}>Paid Leave</option>
                                        <option value="0" {{ $leave->leave_without_pay === "0" ? "Selected" : "" }}>Leave Without Pay</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Leave Schedule</label>
                                    <select class="form-control s2" name="leave_schedule">
                                        <option value="0" {{ $leave->leave_schedule === 0 ? "Selected" : "" }}>Whole Day Leave</option>
                                        <option value="1" {{ $leave->leave_schedule === 0 ? "Selected" : "" }}>Half Day Leave</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Cancel</span>
                                </button>
                                <button type="submit" class="btn btn-warning ml-1">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Save Changes</span>
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        @endforeach

    @foreach($leaves as $leave)
        <div class="modal fade text-left" id="delete{{$leave->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/leaves/{{ $leave->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @endsection

    @section('scripts')
        <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>

        <script>

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('#leaveTable').dataTable({
                processing: true,
                serverSide: true,
                filter: true,
                ajax: "{{ url("/ajax/fetchLeaveTable") }}",
                columns: [
                    {
                        data: "id",
                        name: "id"
                    },
                    {
                        data: 'company',
                        name: 'company',
                    },
                    {
                        data: "employee",
                        name: "employee",
                    },
                    {
                        data: 'leave_type_code',
                        name: 'leave_type_code',
                        "render": function (data, type, row) {
                            switch (data) {

                                case "sl":
                                    return "Sick Leave";

                                case "vl":
                                    return "Vacation Leave";

                                case "materinity":
                                    return "Maternity Leave";

                                case "paternity":
                                    return "Paterity Leave";

                                case "birthday":
                                    return "Birthday Leave";

                                case "service":
                                    return "Service Incentive Leave";

                                case "parental":
                                    return "Parental Leave";

                                case "rehabilitation":
                                    return "Rehabilitation Leave";

                                case "study":
                                    return "Study Leave";

                            }
                        }
                    },
                    {
                        data: "half_day",
                        name: "half_day",
                        "render" : function (data, type, row) {
                            if (data === "0"){
                                return row['leave_amount'] + " Day(s)" +  "<br /><label class='badge badge-success'>Whole Day</label>";
                            } else {
                                return row['leave_amount'] + " Day(s)" +  "<br /><label class='badge badge-warning'>Half Day</label>";
                            }
                        }
                    },
                    {
                        data: "leave_date",
                        name: "leave_date",
                        "render" : function (data, type, row) {
                            console.log(row);
                            if (row['leave_without_pay'] === "0"){
                                return data + "<br /><label class='badge badge-danger'>Leave Without Pay</label>"
                            } else {
                                return data + "<br /><label class='badge badge-success'>Leave With Pay</label>"
                            }
                        }
                    },

                    {
                        data: "filling_date",
                        name: "filling_date"
                    },
                    {
                        data: "reason",
                        name: "reason"
                    },
                    {
                        data: "remarks",
                        name: "remarks"
                    },
                    {
                        data: "status",
                        name: "status",
                        "render": function (data) {

                            if (data === "Pending") {
                                return "<label class='badge badge-warning'>Pending</label>";
                            }
                            else if(data === "Approved") {
                                return "<label class = 'badge badge-success'> Approved</label>";
                            } else {
                                return "<label class = 'badge badge-danger'> Declined</label>";
                            }
                        }
                    },
                    {
                        data: "id",
                        name: "id",
                        "render": function (data, type, row) {

                            let html = '';
                            if(row['status'] === "Pending") {
                                html = "<button type=\"button\" class=\"btn btn-icon rounded-circle btn-warning\"\n" +
                                    "  data-toggle=\"modal\" data-target=\"#update" + data +"\">\n" +
                                    "  <i class=\"bx bx-pencil\"></i>\n" +
                                    "  </button>" +  "<button type=\"button\" class=\"btn btn-icon rounded-circle btn-danger\"\n" +
                                    "  data-toggle=\"modal\" data-target=\"#delete" + data +"\">\n" +
                                    "  <i class=\"bx bx-trash\"></i>\n" +
                                    "  </button>";
                            }


                            return html;

                        }
                    }
                ], initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }

            });


            function checkLeaveCredits() {

                let leave_type = $("#leave_type").val();

                $.get('/ajax/fetchLeaveBalance/' + leave_type, function (response) {
                    console.log(response);
                    $("#parent_balance").css('display', 'block');
                    $("#leave_balance").html(response['balance']);
                })
            }

            function checkLeaveCreditsUpdate() {

                let leave_type = $("#leave_type_update").val();

                $.get('/ajax/fetchLeaveBalance/' + leave_type, function (response) {
                    console.log(response);
                    $("#parent_balance_update").css('display', 'block');
                    $("#leave_balance_update").html(response['balance']);
                })
            }

            $("#leave_form").on("submit", function (e) {
                e.preventDefault();

                $.post('leaves', $("#leave_form").serialize(), function (response) {
                    $('.modal').modal('hide');
                    $("#leave_form")[0].reset();


                    Swal.fire({

                        type: response['type'],
                        title: response['message'],
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000

                    }).then(function () {
                        table.fnDraw();
                    });
                })
            })

            $("#leave_form_update").on("submit", function (e) {
                e.preventDefault();

                $.ajax({
                    url: $("#leave_form_update").attr('action'),
                    type: 'PATCH',
                    data: $("#leave_form_update").serialize(),
                    success: function(result) {
                        // Do something with the result
                        console.log(result);
                    }
                });

                $.post($("#leave_form_update").attr('action'), $("#leave_form_update").serialize(), function (response) {
                    $('.modal').modal('hide');
                    $("#leave_form_update")[0].reset();


                    Swal.fire({

                        type: response['type'],
                        title: response['message'],
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000

                    }).then(function () {
                        table.fnDraw();
                    });
                })
            })
        </script>
    @endsection
