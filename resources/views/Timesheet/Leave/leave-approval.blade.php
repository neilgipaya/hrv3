@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">

            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Leave Approval</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="leaveTable">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Leave Type</th>
                                                <th>Duration</th>
                                                <th>Requested Date</th>
                                                <th>Date of Application</th>
                                                <th>Reason</th>
                                                <th>Remarks</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($leaves as $leave)
                                                    <?php
                                                    $approver_check = explode(',', $leave->approver_id);
                                                    ?>
                                                    @if(in_array(auth()->user()->id, $approver_check))
                                                        <tr>
                                                            <td>{{ $leave->id }}</td>
                                                            <td>{{ $leave->users->jobDetails->company->company_name }}</td>
                                                            <td>{{ $leave->users->lastname . " " . $leave->users->firstname }}</td>
                                                            <td>{{ $leave->leave_type_code }}</td>
                                                            <td>
                                                                @if ($leave->half_day === "0")
                                                                   {{ $leave->leave_amount }} Day(s) <br /><label class='badge badge-success'>Whole Day</label>
                                                                @else
                                                                    {{ $leave->leave_amount }} Day(s) <br /><label class='badge badge-warning'>Half Day</label>
                                                                @endif
                                                            </td>
                                                           <td>
                                                               @if ($leave->without_pay === "0")
                                                                   {{ $leave->from_date }} - {{ $leave->to_date }}<br /><label class='badge badge-danger'>Leave Without Pay</label>
                                                               @else
                                                                   {{ $leave->from_date }}  - {{ $leave->to_date }} <br /><label class='badge badge-warning'>Leave with Pay</label>
                                                               @endif
                                                           </td>
                                                            <td>{{ $leave->filling_date }}</td>
                                                            <td>{{ $leave->reason }}</td>
                                                            <td>{{ $leave->remarks }}</td>
                                                            <td>
                                                                @if($leave->status === "Pending")
                                                                    <label class="badge badge-danger">Pending</label>
                                                                @else
                                                                    <label class="badge badge-success">Approved</label>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                        data-toggle="modal" data-target="#approval{{ $leave->id }}">
                                                                    <i class="bx bx-check-circle"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                        data-toggle="modal" data-target="#decline{{ $leave->id }}">
                                                                    <i class="bx bx-window-close"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>id</th>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Leave Type</th>
                                                <th>Duration</th>
                                                <th>Requested Date</th>
                                                <th>Date of Application</th>
                                                <th>Reason</th>
                                                <th>Remarks</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    @foreach($leaves as $leave)
        <div class="modal fade text-left" id="approval{{$leave->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/leaves/{{ $leave->id }}" method="POST">
                        @csrf
                        @method('PATCH')

                        <div class="modal-header bg-success">
                            <h5 class="modal-title white" id="myModalLabel160">Approve Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="approval" />
                            <p>Are you sure you want to Approve this record?</p>
                            <input type="hidden" name="status" value="Approved" />
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea class="form-control" name="remarks" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-success ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Approve</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($leaves as $leave)
        <div class="modal fade text-left" id="decline{{$leave->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/leaves/{{ $leave->id }}" method="POST">
                        @csrf
                        @method('PATCH')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Decline Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="decline" />
                            <p>Are you sure you want to Decline this record?</p>
                            <input type="hidden" name="status" value="Declined" />
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea class="form-control" name="remarks" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Decline</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let table = $('#leaveTable').dataTable({
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }

        });


        function checkLeaveCredits() {
            let leave_type = $("#leave_type").val();
            $.get('/ajax/fetchLeaveBalance/' + leave_type, function (response) {
                console.log(response);
                $("#parent_balance").css('display', 'block');
                $("#leave_balance").html(response['balance']);
            })
        }

        $("#leave_form").on("submit", function (e) {
            e.preventDefault();

            $.post('leaves', $("#leave_form").serialize(), function (response) {
                $('.modal').modal('hide');
                $("#leave_form")[0].reset();


                Swal.fire({

                    type: response['type'],
                    title: response['message'],
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000

                }).then(function () {
                    table.fnDraw();
                });
            })
        })

        $("#leave_form_update").on("submit", function (e) {
            e.preventDefault();

            $.ajax({
                url: $("#leave_form_update").attr('action'),
                type: 'PATCH',
                data: $("#leave_form_update").serialize(),
                success: function(result) {
                    // Do something with the result
                    console.log(result);
                }
            });

            $.post($("#leave_form_update").attr('action'), $("#leave_form_update").serialize(), function (response) {
                $('.modal').modal('hide');
                $("#leave_form_update")[0].reset();


                Swal.fire({

                    type: response['type'],
                    title: response['message'],
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000

                }).then(function () {
                    table.fnDraw();
                });
            })
        })
    </script>
@endsection
