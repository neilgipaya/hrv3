@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->

            @can('import-ob')
            <section id="basic-import">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Employee OB</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <form action="/timesheet/import-ob" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label>OB CSV</label>
                                            <input type="file" class="form-control" name="import_file" required />
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-danger">Upload Data</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endcan
            <!--/ Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >OB List</h4>
                                </div>
                                <div style="float: right;">
                                    <a href="#" data-toggle="modal" data-target="#create" class="btn btn-primary"> Create New</a>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="sortTable">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Filling Date</th>
                                                <th>OB Date</th>
                                                <th>OB Hours</th>
                                                <th>Total Hours</th>
                                                <th>Travel Distance</th>
                                                <th>Transportation Amount</th>
                                                <th>Grab Allowance</th>
                                                <th>Meals</th>
                                                <th>Others</th>
                                                <th>Reason</th>
                                                <th>Attachment</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($obs as $row)
                                                <tr>
                                                    <td>{{ $row->users->jobDetails->company->company_name }}</td>
                                                    <td>{{ $row->users->lastname . " " . $row->users->firstname }}</td>
                                                    <td>{{ $row->filling_date }}</td>
                                                    <td>{{ $row->start_date . " - " .  $row->end_date }}</td>
                                                    <td>{{ $row->start_time . " - " .  $row->end_time }}</td>
                                                    <td>{{ $row->num_hours }}</td>
                                                    <td>{{ $row->travel_distance }}</td>
                                                    <td>{{ $row->transportation_amount }}</td>
                                                    <td>{{ $row->grab }}</td>
                                                    <td>{{ $row->meals }}</td>
                                                    <td>{{ $row->others }}</td>
                                                    <td>{{ $row->reason }}</td>
                                                    <td>
                                                        <a href="{{ asset('storage/attachments/' . $row->attachment) }}" class="btn btn-light-primary"> <i class="bx bx-cloud-download"></i></a>
                                                    </td>
                                                    <td>
                                                        @if ($row->status === "Pending")
                                                            <label class='badge badge-warning'>Pending</label>
                                                        @elseif($row->status === "Approved")
                                                            <label class = 'badge badge-success'> Approved</label>
                                                        @else
                                                            <label class = 'badge badge-danger'> Declined</label>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if($row->status === "Pending")
                                                            <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                    data-toggle="modal" data-target="#update{{ $row->id }}">
                                                                <i class="bx bx-pencil"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                    data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                                <i class="bx bx-trash"></i>
                                                            </button>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Filling Date</th>
                                                <th>OB Date</th>
                                                <th>OB Hours</th>
                                                <th>Total Hours</th>
                                                <th>Travel Distance</th>
                                                <th>Transportation Amount</th>
                                                <th>Grab Allowance</th>
                                                <th>Meals</th>
                                                <th>Others</th>
                                                <th>Reason</th>
                                                <th>Status</th>
                                                <th>Attachment</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <!--/ Zero configuration table -->
        </div>
    </div>

    <!--primary theme Modal -->

    <div class="modal fade text-left" id="create"  aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <form action="/timesheet/ob" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">Create Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Start Date</label>
                                <input type="text" class="form-control single-daterange" name="start_date" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>End Date</label>
                                <input type="text" class="form-control single-daterange" name="end_date"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Start Time</label>
                                <input type="time" class="form-control" name="start_time"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>End Time</label>
                                <input type="time" class="form-control" name="end_time" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Travel Distance</label>
                                <input type="text" class="form-control" value="0" name="travel_distance" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Transportation Amount</label>
                                <input type="text" step="any" value="0" class="form-control" name="transportation_amount" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Grab (Optional)</label>
                                <input type="text" step="any" value="0" class="form-control" name="grab" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Purpose</label>
                                <input type="text" class="form-control" name="purpose" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Meals</label>
                                <input type="text" step="any" value="0" class="form-control" name="meals" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Others</label>
                                <input type="text" class="form-control" name="others"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Reason / Remarks</label>
                                <textarea class="form-control" name="reason" rows="3"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Attachment</label>
                            <input type="file" name="file_attachment" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Cancel</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Save</span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    @foreach($obs as $row)

        <div class="modal fade text-left" id="update{{ $row->id }}"  aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/ob/{{ $row->id }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Start Date</label>
                                    <input type="text" class="form-control single-daterange" name="start_date" value="{{ $row->start_date }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>End Date</label>
                                    <input type="text" class="form-control single-daterange" name="end_date" value="{{ $row->end_date }}"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Start Time</label>
                                    <input type="time" class="form-control" name="start_time" value="{{ $row->start_time }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>End Time</label>
                                    <input type="time" class="form-control" name="end_time" value="{{ $row->end_time }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Travel Distance</label>
                                    <input type="text" class="form-control" value="{{ $row->travel_distance }}" name="travel_distance" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Transportation Amount</label>
                                    <input type="text" step="any" value="{{ $row->transportation_amount }}" class="form-control" name="transportation_amount" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Grab (Optional)</label>
                                    <input type="text" step="any" value="{{ $row->grab }}" class="form-control" name="grab" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Purpose</label>
                                    <input type="text" class="form-control" value="{{ $row->purpose }}" name="purpose" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Meals</label>
                                    <input type="text" step="any" value="{{ $row->meals }}" class="form-control" name="meals" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Others</label>
                                    <input type="text" class="form-control" name="others"  value="{{ $row->others }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Reason / Remarks</label>
                                    <textarea class="form-control" name="reason" rows="3"
                                              required>{{ $row->reason }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Attachment (Upload a file if you want to replace the existing one)</label>
                                <input type="file" name="file_attachment" class="form-control" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-warning ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Save Changes</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($obs as $row)
        <div class="modal fade text-left" id="delete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/ob/{{ $row->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach


@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
