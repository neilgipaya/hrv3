@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">

            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >OB Approval</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="sortTable">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Filling Date</th>
                                                <th>OB Date</th>
                                                <th>OB Hours</th>
                                                <th>Total Hours</th>
                                                <th>Travel Distance</th>
                                                <th>Transportation Amount</th>
                                                <th>Grab Allowance</th>
                                                <th>Meals</th>
                                                <th>Others</th>
                                                <th>Reason</th>
                                                <th>Attachment</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($obs as $row)
                                                <?php
                                                $approver_check = explode(',', $row->approver_id);
                                                ?>
                                                @if(in_array(auth()->user()->id, $approver_check))
                                                    <tr>
                                                        <td>{{ $row->users->jobDetails->company->company_name }}</td>
                                                        <td>{{ $row->users->lastname . " " . $row->users->firstname }}</td>
                                                        <td>{{ $row->filling_date }}</td>
                                                        <td>{{ $row->start_date . " - " .  $row->end_date }}</td>
                                                        <td>{{ $row->start_time . " - " .  $row->end_time }}</td>
                                                        <td>{{ $row->num_hours }}</td>
                                                        <td>{{ $row->travel_distance }}</td>
                                                        <td>{{ $row->transportation_amount }}</td>
                                                        <td>{{ $row->grab }}</td>
                                                        <td>{{ $row->meals }}</td>
                                                        <td>{{ $row->others }}</td>
                                                        <td>{{ $row->reason }}</td>
                                                        <td>
                                                            <a href="{{ asset('storage/attachments/' . $row->attachment) }}" class="btn btn-light-primary"> <i class="bx bx-cloud-download"></i></a>
                                                        </td>
                                                        <td>
                                                            @if ($row->status === "Pending")
                                                                <label class='badge badge-warning'>Pending</label>
                                                            @elseif($row->status === "Approved")
                                                                <label class = 'badge badge-success'> Approved</label>
                                                            @else
                                                                <label class = 'badge badge-danger'> Declined</label>
                                                            @endif
                                                        </td>

                                                        <td>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                    data-toggle="modal" data-target="#approval{{ $row->id }}">
                                                                <i class="bx bx-check-circle"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                    data-toggle="modal" data-target="#decline{{ $row->id }}">
                                                                <i class="bx bx-window-close"></i>
                                                            </button>

                                                        </td>

                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Company</th>
                                                <th>Employee</th>
                                                <th>Filling Date</th>
                                                <th>OB Date</th>
                                                <th>OB Hours</th>
                                                <th>Total Hours</th>
                                                <th>Travel Distance</th>
                                                <th>Transportation Amount</th>
                                                <th>Grab Allowance</th>
                                                <th>Meals</th>
                                                <th>Others</th>
                                                <th>Reason</th>
                                                <th>Status</th>
                                                <th>Attachment</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    <!--primary theme Modal -->

    @foreach($obs as $row)
        <div class="modal fade text-left" id="approval{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/ob/{{ $row->id }}" method="POST">
                        @csrf
                        @method('PATCH')

                        <div class="modal-header bg-success">
                            <h5 class="modal-title white" id="myModalLabel160">Approve Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="approval" />
                            <p>Are you sure you want to Approve this record?</p>
                            <input type="hidden" name="status" value="Approved" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-success ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Approve</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($obs as $row)
        <div class="modal fade text-left" id="decline{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/timesheet/ob/{{ $row->id }}" method="POST">
                        @csrf
                        @method('PATCH')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Decline Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="approval" />
                            <p>Are you sure you want to Decline this record?</p>
                            <input type="hidden" name="status" value="Declined" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Decline</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
