@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Attendance</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="export_table">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Employee No</th>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Department</th>
                                                <th>Cost Center</th>
                                                <th>Time In</th>
                                                <th>Time Out</th>
                                                <th>Late</th>
                                                <th>Undertime</th>
                                                <th>Overtime</th>
                                                <th>Total Work Hours</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($attendance as $row)
                                                <tr>
                                                    <td>{{ $row->employee->jobDetails->company->company_name }}</td>
                                                    <td>{{ $row->employee->employee_id }}</td>
                                                    <td>{{ $row->employee->lastname . " " . $row->employee->firstname . " " . $row->employee->middlename[0] }}</td>
                                                    <td>{{ $row->employee->jobDetails->job_title }}</td>
                                                    <td>{{ $row->employee->jobDetails->department->department_name }}</td>
                                                    <td>{{ $row->employee->jobDetails->costCenter->cost_center_name }}</td>
                                                    <td>{{ date('H:i', strtotime($row->time_in)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->time_out)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->late)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->undertime)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->overtime)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->total_work_hours)) }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
