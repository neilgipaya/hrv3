@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">

            <section id="attendance-form">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Filter Attendance</h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <form action="/timesheet/attendance-update" method="GET">
                                    <div class="form-group">
                                        <label>Date From</label>
                                        <input type="text" class="form-control single-daterange" required name="date_from" />
                                    </div>
                                    <div class="form-group">
                                        <label>Date To</label>
                                        <input type="text" class="form-control single-daterange" required name="date_to" />
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-warning btn-block"><i class="bx bx-filter"></i> Filter</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- TODO:  MAKE THIS AJAX FORMAT -->
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Attendance</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="sortTable">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Employee No</th>
                                                <th>Name</th>
                                                <th>Time In</th>
                                                <th>Time Out</th>
                                                <th>Late</th>
                                                <th>Undertime</th>
                                                <th>Overtime</th>
                                                <th>Total Work Hours</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($attendances as $row)
                                                @if(isset($row->employee->lastname))
                                                <tr>
                                                    <td>{{ $row->employee->jobDetails->company->company_name ?? "NONE" }}</td>
                                                    <td>{{ $row->employee->employee_id ?? "NONE" }}</td>
                                                    <td>{{ $row->employee->lastname  . " " . $row->employee->firstname . " " . $row->employee->middlename[0] ?? "NONE" }}</td>
                                                    <td>{{ date('H:i', strtotime($row->time_in_work)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->time_out_work)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->tardiness)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->undertime)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->overtime)) }}</td>
                                                    <td>{{ date('H:i', strtotime($row->total_work_hours)) }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                data-toggle="modal" data-target="#update{{ $row->id }}">
                                                            <i class="bx bx-pencil"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>

    @foreach($attendances as $row)
        <div class="modal fade text-left" id="update{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/timesheet/attendance-save/{{ $row->id }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="update">
                            <div class="form-group">
                                <label>Attendance Date</label>
                                <input type="date" class="form-control required" name="attendance_date" value="{{ $row->attendance_date }}" required />
                            </div>
                            <div class="form-group">
                                <label>Time - in</label>
                                <input type="time" class="form-control" name="time_in_work" value="{{ $row->time_in_work }}" required />
                            </div>
                            <div class="form-group">
                                <label>Time - out</label>
                                <input type="time" class="form-control" name="time_out_work" value="{{ $row->time_out_work }}" required />
                            </div>
                            <div class="form-group">
                                <label>Overtime</label>
                                <input type="time" class="form-control" name="overtime" value="{{ $row->overtime }}" required />
                            </div>
                            <div class="form-group">
                                <label>Undertime</label>
                                <input type="time" class="form-control" name="undertime" value="{{ $row->undertime }}" required />
                            </div>
                            <div class="form-group">
                                <label>Late</label>
                                <input type="time" class="form-control" name="tardiness" value="{{ $row->tardiness }}" required />
                            </div>
                            <div class="form-group">
                                <label>Total Work</label>
                                <input type="time" class="form-control" name="total_work" value="{{ $row->total_work }}" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-warning ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Save Changes</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
