@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Payroll List</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Cut off</th>
                                                <th>Description</th>
                                                <th>SSS</th>
                                                <th>PAGIBIG</th>
                                                <th>PHILHEALTH</th>
                                                <th>TAX</th>
                                                <th>LOAN</th>
                                                <th>Run Type</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($payrolls as $row)
                                                <tr>
                                                    <td>{{ $row->from . " - " . $row->to }}</td>
                                                    <td>{{ $row->description }}</td>
                                                    <td>{{ $row->compute_sss }}</td>
                                                    <td>{{ $row->compute_pagibig }}</td>
                                                    <td>{{ $row->compute_philhealth }}</td>
                                                    <td>{{ $row->compute_tax }}</td>
                                                    <td>{{ $row->compute_loan }}</td>
                                                    <td>{{ $row->payroll_run_type }}</td>
                                                    <td>
                                                        <a href="{{ url('payroll/payroll-list/' . $row->id) }}" class="btn btn-icon rounded-circle btn-warning" title="View Payroll Processed">
                                                            <i class="bx bx-money"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
