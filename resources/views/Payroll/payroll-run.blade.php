@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Form wizard with step validation section start -->
            <section id="validation">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">Payroll Run</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="/payroll/payroll-run" method="POST" class="wizard-validation-payroll">
                                        @csrf
                                        <!-- Step 1 -->
                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Payroll Setup</span>
                                        </h6>
                                        <!-- Step 1 -->
                                        <!-- body content of step 1 -->
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Payroll Run Type</label>
                                                        <select class="form-control" name="run_type" required>
                                                            <option value="0">Normal Payroll</option>
                                                            <option value="1">Special Run Payroll</option>
                                                            <option value="2">Adjustments Run Payroll</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Transaction Date</label>
                                                        <input type="text" class="form-control single-daterange" name="transaction_date" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Pay Group</label>
                                                        <select class="form-control" name="pay_groups" required>
                                                            @foreach($pay_groups as $pay_group)
                                                                <option value="{{ $pay_group->id }}">{{ $pay_group->pg_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Period</label>
                                                        <select class="form-control" name="period" required>
                                                            <option value="1">1st cut-off</option>
                                                            <option value="2">2nd cut-off</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Billing Template</label>
                                                        <select class="form-control" name="billing" required>
                                                            @foreach($billings as $billing)
                                                                <option value="{{ $billing->id }}">{{ $billing->date_from . " - " . $billing->date_to }} - {{ $billing->description }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea class="form-control" name="description"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- Step 1 -->
                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Deductions</span>
                                        </h6>
                                        <!-- Step 1 -->
                                        <!-- body content of step 1 -->
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Compute SSS</label>
                                                        <select class="form-control" name="compute_sss" required>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                            <option value="Full">Full</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Compute PAGIBIG</label>
                                                        <select class="form-control" name="compute_pagibig" required>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                            <option value="Full">Full</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Compute Philhealth</label>
                                                        <select class="form-control" name="compute_philhealth" required>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                            <option value="Full">Full</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Compute Tax</label>
                                                        <select class="form-control" name="compute_tax" required>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                            <option value="Full">Full</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Compute Loan</label>
                                                        <select class="form-control" name="compute_loan" required>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                            <option value="Full">Full</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- Step 1 -->
                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Employees</span>
                                        </h6>
                                        <!-- Step 1 -->
                                        <!-- body content of step 1 -->
                                        <fieldset>
                                            <div class="row">
                                                <span>You can select either company or specific employees</span>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Select Employees</label>
                                                        <select class="form-control s2" name="employees[]" multiple >
                                                            @foreach($employees as $employee)
                                                                <option value="{{ $employee->id }}">{{ $employee->employee_id }} - {{ $employee->firstname . " " . $employee->lastname}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label>Select Company</label>--}}
{{--                                                        <select class="form-control" name="company" >--}}
{{--                                                            <option value=""></option>--}}
{{--                                                            @foreach($companies as $company)--}}
{{--                                                                <option value="{{ $company->id }}">{{ $company->company_name }}</option>--}}
{{--                                                            @endforeach--}}
{{--                                                        </select>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/forms/wizard-steps.js"></script>


    <script>
        $(".money").inputmask({ alias : "currency" });
        $(".number").inputmask({ alias: "numeric" });


        function checkCompany() {

            let company = $("#company").val();

            let cost_center = $("#cost_center").empty();
            let department = $("#department").empty();


            $.get('/ajax/fetchCostCenter/' + company, function (response) {

                $('<option value="">' + "-- Select Record --" + '</option>').appendTo(cost_center);

                $.each(response, function(i, item) {
                    console.log(item['id']);
                    $('<option value="' + item['id'] + '">' + item['cost_center_name'] + '</option>').
                    appendTo(cost_center);
                });

            });

            $.get('/ajax/fetchDepartment/' + company, function (response) {

                $('<option value="">' + "-- Select Record --" + '</option>').appendTo(department);

                $.each(response, function(i, item) {
                    console.log(item['id']);
                    $('<option value="' + item['id'] + '">' + item['department_name'] + '</option>').
                    appendTo(department);
                });

            });
        }
    </script>
@endsection
