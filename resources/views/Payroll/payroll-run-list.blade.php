@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Payroll List</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="export_table">
                                            <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Monthly Salary</th>
                                                <th>Daily Salary</th>
                                                <th>Hourly Rate</th>
                                                <th>BH</th>
                                                <th>RDH</th>
                                                <th>SH</th>
                                                <th>LH</th>
                                                <th>SHRD</th>
                                                <th>LHRD</th>
                                                <th>DH</th>
                                                <th>DHRD</th>
                                                <th>OT</th>
                                                <th>RDOT</th>
                                                <th>SHOT</th>
                                                <th>LHOT</th>
                                                <th>SH-RD OT</th>
                                                <th>DH OT</th>
                                                <th>DH-RD OT</th>
                                                <th>LATE</th>
                                                <th>TOTAL GROSS</th>
                                                <th>TAXABLE INCOME</th>
                                                <th>COMPANY LOAN</th>
                                                <th>PAGIBIG LOAN</th>
                                                <th>SSS LOAN</th>
                                                <th>SSS DEDUCTIONS</th>
                                                <th>PAGIBIG DEDUCTIONS</th>
                                                <th>HDMF DEDUCTIONS</th>
                                                <th>WITHHOLDING TAX</th>
                                                <th>NET PAY</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($payrolls as $row)
                                                <tr>
                                                    <td>{{ $row->lastname . " - " . $row->firstname }}</td>
                                                    <td>{{ number_format($row->monthly_salary, 2) }}</td>
                                                    <td>{{ number_format($row->daily_salary, 2) }}</td>
                                                    <td>{{ number_format($row->hourly_rate, 2) }}</td>
                                                    <td>{{ number_format($row->basic_hours, 2) }}</td>
                                                    <td>{{ number_format($row->rd_hours, 2) }}</td>
                                                    <td>{{ number_format($row->sh_hours, 2) }}</td>
                                                    <td>{{ number_format($row->lh_hours, 2) }}</td>
                                                    <td>{{ number_format($row->sh_rd_hours, 2) }}</td>
                                                    <td>{{ number_format($row->lh_rd_hours, 2) }}</td>
                                                    <td>{{ number_format($row->dh_hours, 2) }}</td>
                                                    <td>{{ number_format($row->dh_rd_hours, 2) }}</td>
                                                    <td>{{ number_format($row->overtime, 2) }}</td>
                                                    <td>{{ number_format($row->rd_ot_hours, 2) }}</td>
                                                    <td>{{ number_format($row->sh_ot_hours, 2) }}</td>
                                                    <td>{{ number_format($row->lh_ot_hours, 2) }}</td>
                                                    <td>{{ number_format($row->sh_rd_ot_hours, 2) }}</td>
                                                    <td>{{ number_format($row->dh_ot_hours, 2) }}</td>
                                                    <td>{{ number_format($row->dh_rd_ot_hours, 2) }}</td>
                                                    <td>{{ number_format($row->late, 2) }}</td>
                                                    <td>{{ number_format($row->total_gross, 2) }}</td>
                                                    <td>{{ number_format($row->taxable_income, 2) }}</td>
                                                    <td>{{ number_format($row->total_company_loan, 2) }}</td>
                                                    <td>{{ number_format($row->total_pagibig_loan, 2) }}</td>
                                                    <td>{{ number_format($row->total_sss_loan, 2) }}</td>
                                                    <td>{{ number_format($row->sss_deduction, 2) }}</td>
                                                    <td>{{ number_format($row->pagibig_deduction, 2) }}</td>
                                                    <td>{{ number_format($row->philhealth_deduction, 2) }}</td>
                                                    <td>{{ number_format($row->withholding_tax, 2) }}</td>
                                                    <td>{{ number_format($row->total_net_pay, 2) }}</td>
                                                    <th></th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
