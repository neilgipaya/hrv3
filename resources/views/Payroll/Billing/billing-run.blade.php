@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">

            <section id="basic-import">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Process Billing</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <form action="/payroll/billing" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label>Date From</label>
                                            <input type="text" class="form-control single-daterange" name="date_from" required />
                                        </div>
                                        <div class="form-group">
                                            <label>Date To</label>
                                            <input type="text" class="form-control single-daterange" name="date_to" required />
                                        </div>
                                        <div class="form-group">
                                            <label>Company</label>
                                            <select class="form-control s2" name="company_id" required>
                                                @foreach($companies as $company)
                                                    <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="description" rows="4"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-danger">Process Billing</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Billing List</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="sortTable">
                                            <thead>
                                            <tr>
                                                <th>Covered Date</th>
                                                <th>Company</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($billing_headers as $row)
                                                <tr>
                                                    <td>{{ $row->date_from . " - " . $row->date_to }}</td>
                                                    <td>{{ $row->company->company_name }}</td>
                                                    <td>{{ $row->description }}</td>
                                                    <td>{{ $row->status }}</td>
                                                    <td>
                                                        <a href="{{ url('payroll/billing-list/' . $row->id) }}" class="btn btn-icon rounded-circle btn-warning" title="View Billing Processed">
                                                            <i class="bx bx-money"></i>
                                                        </a>
                                                        @can('billing-approve')
                                                            <button type="button" class="btn btn-icon rounded-circle btn-success"
                                                                    data-toggle="modal" data-target="#approve{{ $row->id }}" data-title="Approve">
                                                                <i class="bx bx-check-circle"></i>
                                                            </button>
                                                        @endcan
                                                        @can('billing-delete')
                                                            <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                    data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                                <i class="bx bx-trash"></i>
                                                            </button>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    <!--primary theme Modal -->
    @foreach($billing_headers as $row)
        <div class="modal fade text-left" id="approve{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/payroll/billing-header/{{ $row->id }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title white" id="myModalLabel160">Approve Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                           <input type="hidden" name="status" value="Approved" />
                            <p>Are you sure you want to approve this billing? </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-warning ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Approve</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($billing_headers as $row)
        <div class="modal fade text-left" id="delete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <form action="/payroll/billing-header/{{ $row->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
