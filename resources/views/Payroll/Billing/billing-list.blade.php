@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Billing List</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table" id="billingTable">
                                            <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Daily Rate</th>
                                                <th>RH</th>
                                                <th>PHP RH</th>
                                                <th>LATE</th>
                                                <th>PHP LATE</th>
                                                <th>ABSENT</th>
                                                <th>PHP ABSENT</th>
                                                <th>UNDERTIME</th>
                                                <th>PHP UNDERTIME</th>
                                                <th>BASIC PAY</th>
                                                <th>NSD</th>
                                                <th>PHP NSD</th>
                                                <th>NSD2</th>
                                                <th>PHP NSD2</th>
                                                <th>ROT</th>
                                                <th>PHP ROT</th>
                                                <th>RDOT</th>
                                                <th>PHP RDOT</th>
                                                <th>RDOTe</th>
                                                <th>PHP RDOTe</th>
                                                <th>SHOT</th>
                                                <th>PHP SHOT</th>
                                                <th>SHOTe</th>
                                                <th>PHP SHOTe</th>
                                                <th>SHRD</th>
                                                <th>PHP SHRD</th>
                                                <th>SHRDe</th>
                                                <th>PHP SHRDe</th>
                                                <th>LHOT</th>
                                                <th>PHP LHOT</th>
                                                <th>LHOTe</th>
                                                <th>PHP LHOTe</th>
                                                <th>LHRD</th>
                                                <th>PHP LHRD</th>
                                                <th>LHRDe</th>
                                                <th>PHP LHRDe</th>
                                                <th>HOLIDAY PAY</th>
                                                <th>PHP HOLIDAY PAY</th>
                                                <th>TOTAL OT</th>
                                                <th>COLA</th>
                                                <th>GROSS PAY</th>
                                                <th>TOTAL BASIC PAY (Current & Previous Cut - off)</th>
                                                <th>ER SSS</th>
                                                <th>ER EC</th>
                                                <th>ER PHIC</th>
                                                <th>ER HDMF</th>
                                                <th>ER ALLOWANCE</th>
                                                <th>TOTAL LABOR</th>
                                                <th>SIL</th>
                                                <th>13th MONTH PAY</th>
                                                <th>SEPARATION</th>
                                                <th>PPE</th>
                                                <th>TOTAL</th>
                                                <th>ADMIN</th>
                                                <th>GRAND TOTAL</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($billings as $row)
                                                <tr>
                                                    <td>{{ $row->user->firstname . " " . $row->user->lastname }}</td>
                                                    <td>{{ number_format($row->daily_rate, 2) }}</td>
                                                    <td>{{ number_format($row->rh, 2) }}</td>
                                                    <td>{{ number_format($row->php_rh, 2) }}</td>
                                                    <td>{{ number_format($row->late, 2) }}</td>
                                                    <td>{{ number_format($row->php_late, 2) }}</td>
                                                    <td>{{ number_format($row->absent, 2) }}</td>
                                                    <td>{{ number_format($row->php_absent, 2) }}</td>
                                                    <td>{{ number_format($row->undertime, 2) }}</td>
                                                    <td>{{ number_format($row->php_undertime, 2) }}</td>
                                                    <td>{{ number_format($row->basic_pay, 2) }}</td>
                                                    <td>{{ number_format($row->nsd, 2) }}</td>
                                                    <td>{{ number_format($row->php_nsd, 2) }}</td>
                                                    <td>{{ number_format($row->nsd2, 2) }}</td>
                                                    <td>{{ number_format($row->php_nsd2, 2) }}</td>
                                                    <td>{{ number_format($row->rot, 2) }}</td>
                                                    <td>{{ number_format($row->php_rot, 2) }}</td>
                                                    <td>{{ number_format($row->rdot, 2) }}</td>
                                                    <td>{{ number_format($row->php_rdot, 2) }}</td>
                                                    <td>{{ number_format($row->rdote, 2) }}</td>
                                                    <td>{{ number_format($row->php_rdote, 2) }}</td>
                                                    <td>{{ number_format($row->shot, 2) }}</td>
                                                    <td>{{ number_format($row->php_shot, 2) }}</td>
                                                    <td>{{ number_format($row->shote, 2) }}</td>
                                                    <td>{{ number_format($row->php_shote, 2) }}</td>
                                                    <td>{{ number_format($row->shrd, 2) }}</td>
                                                    <td>{{ number_format($row->php_shrd, 2) }}</td>
                                                    <td>{{ number_format($row->shrde, 2) }}</td>
                                                    <td>{{ number_format($row->php_shrde, 2) }}</td>
                                                    <td>{{ number_format($row->lhot, 2) }}</td>
                                                    <td>{{ number_format($row->php_lhot, 2) }}</td>
                                                    <td>{{ number_format($row->lhote, 2) }}</td>
                                                    <td>{{ number_format($row->php_lhote, 2) }}</td>
                                                    <td>{{ number_format($row->lhrd, 2) }}</td>
                                                    <td>{{ number_format($row->php_lhrd, 2) }}</td>
                                                    <td>{{ number_format($row->lhrde, 2) }}</td>
                                                    <td>{{ number_format($row->php_lhrde, 2) }}</td>
                                                    <td>{{ number_format($row->holiday, 2) }}</td>
                                                    <td>{{ number_format($row->php_holiday, 2) }}</td>
                                                    <td>{{ number_format($row->php_total_ot, 2) }}</td>
                                                    <td>{{ number_format($row->cola, 2) }}</td>
                                                    <td>{{ number_format($row->gross_pay, 2) }}</td>
                                                    <td>{{ number_format($row->total_basic_pay, 2) }}</td>
                                                    <td>{{ number_format($row->er_sss, 2) }}</td>
                                                    <td>{{ number_format($row->er_ec, 2) }}</td>
                                                    <td>{{ number_format($row->er_phic, 2) }}</td>
                                                    <td>{{ number_format($row->er_hdmf, 2) }}</td>
                                                    <td>{{ number_format($row->allowance, 2) }}</td>
                                                    <td>{{ number_format($row->total_labor, 2) }}</td>
                                                    <td>{{ number_format($row->sil, 2) }}</td>
                                                    <td>{{ number_format($row->th_month, 2) }}</td>
                                                    <td>{{ number_format($row->separation, 2) }}</td>
                                                    <td>{{ number_format($row->ppe, 2) }}</td>
                                                    <td>{{ number_format($row->total, 2) }}</td>
                                                    <td>{{ number_format($row->admin, 2) }}</td>
                                                    <td>{{ number_format($row->grand_total, 2) }}</td>
                                                    <th>
                                                        @can('billing-edit')
                                                            <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                    data-toggle="modal" data-target="#update{{ $row->id }}">
                                                                <i class="bx bx-pencil"></i>
                                                            </button>
                                                        @endcan
                                                        @can('billing-delete')
                                                            <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                    data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                                <i class="bx bx-trash"></i>
                                                            </button>
                                                        @endcan
                                                    </th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    @foreach($billings as $row)
        <div class="modal fade text-left" id="update{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/payroll/billing/{{ $row->id }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" value="{{ $row->total_labor }}" name="total_labor" />
                            <div class="form-group">
                                <label>SIL</label>
                                <input type="text" class="form-control required number" name="sil" value="{{ $row->sil }}"/>
                            </div>

                            <div class="form-group">
                                <label>13th Month</label>
                                <input type="text" class="form-control required number" name="th_month" value="{{ $row->th_month }}"/>
                            </div>

                            <div class="form-group">
                                <label>Separation</label>
                                <input type="text" class="form-control required number" name="separation" value="{{ $row->separation }}"/>
                            </div>

                            <div class="form-group">
                                <label>PPE</label>
                                <input type="text" class="form-control required number" name="ppe" value="{{ $row->ppe }}"/>
                            </div>

                            <div class="form-group">
                                <label>Admin</label>
                                <input type="text" class="form-control required number" name="admin" value="{{ $row->admin }}"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-warning ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Save Changes</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($billings as $row)
        <div class="modal fade text-left" id="delete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/payroll/billing/{{ $row->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>

    <script>
        $('#billingTable').DataTable( {
            scrollY:        "1100px",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 1,
            }
        } );
    </script>
@endsection
