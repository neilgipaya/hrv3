<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Aviary HR Login</title>
    <link rel="apple-touch-icon" href="{{ asset('app-assets') }}/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets') }}/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/vendors-rtl.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/pages/authentication.css">
    <!-- END: Page CSS-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <section id="auth-login" class="row flexbox-container">
                <div class="col-xl-8 col-11">
                    <div class="card bg-authentication mb-0">
                        <div class="row m-0">

                            <!-- right section image -->
                            <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                <div class="card-content">
                                    <img class="img-fluid" src="../../../app-assets/images/pages/login.png" alt="branding logo">
                                </div>
                            </div>
                            <!-- left section-login -->
                            <div class="col-md-6 col-12 px-0">
                                <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="text-center mb-2">Welcome to Aviary HR</h4>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="divider">
                                                <div class="divider-text text-uppercase text-muted"><small>Login to your Account</small>
                                                </div>
                                            </div>
                                            <form method="POST" id="login_form">

                                                <div class="form-group mb-50">

                                                    <label class="text-bold-600" for="exampleInputEmail1">Username</label>
                                                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username" required>

                                                </div>

                                                <div class="form-group">

                                                    <label class="text-bold-600" for="exampleInputPassword1">Password</label>
                                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>

                                                </div>

                                                <div class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">

                                                    <div class="text-left">
                                                        <div class="checkbox checkbox-sm">
                                                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember">
                                                            <label class="checkboxsmall" for="exampleCheck1"><small>Keep me logged
                                                                    in</small></label>
                                                        </div>
                                                    </div>

                                                    <div class="text-right">
                                                        <a href="auth-forgot-password.html" class="card-link"><small>Forgot Password?</small></a>
                                                    </div>

                                                </div>
                                                <button type="submit" class="btn btn-primary glow w-100 position-relative">
                                                    Login <i id="icon-arrow" class="bx bx-right-arrow-alt"></i>
                                                </button>
                                            </form>
                                            <hr>
                                            <div class="text-center"><small class="mr-25">Don't have an account?</small><a href="auth-register.html"><small>Sign up</small></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- login page ends -->

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="{{ asset('app-assets') }}/vendors/js/vendors.min.js"></script>
<script src="{{ asset('app-assets') }}/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="{{ asset('app-assets') }}/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="{{ asset('app-assets') }}/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>

<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('app-assets') }}/js/scripts/configs/vertical-menu-light.js"></script>
<script src="{{ asset('app-assets') }}/js/core/app-menu.js"></script>
<script src="{{ asset('app-assets') }}/js/core/app.js"></script>
<script src="{{ asset('app-assets') }}/js/scripts/components.js"></script>
<script src="{{ asset('app-assets') }}/js/scripts/footer.js"></script>

<script src="{{ asset('app-assets') }}/scripts/authentication/login.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
