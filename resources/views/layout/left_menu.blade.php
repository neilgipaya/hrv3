<ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="filled">

    @if(auth()->user()->can('admin-dashboard') or auth()->user()->can('employee-dashboard'))
        <li class="dropdown nav-item {{ Request::segment(1) == 'dashboard' or Request::segment(1) == 'my-dashboard'? 'active': '' }}" data-menu="dropdown">
            <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                <i class="menu-livicon" data-icon="desktop"></i><span>Dashboard</span></a>
            <ul class="dropdown-menu">
                @can('admin-dashboard')
                    <li data-menu="" class="{{ Request::segment(1) == 'dashboard'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('dashboard') }}" data-toggle="dropdown">
                            <i class="bx bx-right-arrow-alt"></i>Admin Dashboard</a>
                    </li>
                @endcan
                @can('employee-dashboard')
                    <li data-menu="" class="{{ Request::segment(1) == 'my-dashboard' ? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('my-dashboard') }}" data-toggle="dropdown">
                            <i class="bx bx-right-arrow-alt"></i>My Dashboard</a>
                    </li>
                @endcan
            </ul>
        </li>
    @endif

    @can('employee-list')
    <li class="dropdown nav-item {{ Request::segment(1) == 'employees'? 'active': '' }}" data-menu="dropdown">
        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
            <i class="menu-livicon" data-icon="users"></i><span>Employee</span></a>

        <ul class="dropdown-menu">
            @can('employee-create')
            <li data-menu="" class="{{ Request::segment(2) == 'create'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees/create') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Employee Create Wizard</a>
            </li>
            @endcan
            @can('employee-list')
            <li data-menu="" class="{{ Request::segment(1) == 'employees' && Request::segment(2)  == null ? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Employees</a>
            </li>
            @endcan
            @can('employee-import')
            <li data-menu=""><a class="dropdown-item align-items-center" href="#" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Import Employees</a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

{{--    @can('core-hr')--}}
{{--    <li class="dropdown nav-item {{ Request::segment(1) == 'HR'? 'active': '' }}" data-menu="dropdown">--}}
{{--        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">--}}
{{--            <i class="menu-livicon" data-icon="home"></i><span>Core HR</span></a>--}}

{{--        <ul class="dropdown-menu">--}}
{{--            @can('awards-list')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'awards'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees/create') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Awards</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--            @can('transfer-list')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'transfers' ? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Transfers</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--            @can('resignation-list')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'resignation' ? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Resignation</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--            @can('complaints-list')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'complaints' ? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Complaints</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--            @can('warnings-list')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'warnings' ? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('employees') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Warnings</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--    @endcan--}}


    @can('organization')
    <li class="dropdown nav-item  {{ Request::segment(1) == 'organization'? 'active': '' }}" data-menu="dropdown">
        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
            <i class="menu-livicon" data-icon="diagram"></i><span>Organization</span></a>

        <ul class="dropdown-menu">
            @can('company-list')
            <li data-menu="" class="{{ Request::segment(2) == 'companies'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('organization/companies') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Company</a>
            </li>
            @endcan
            @can('cost-center-list')
            <li data-menu="" class="{{ Request::segment(2) == 'costCenters'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/costCenters') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Cost Centers</a>
            </li>
            @endcan
            @can('departments-list')
            <li data-menu="" class="{{ Request::segment(2) == 'departments'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/departments') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Departments</a>
            </li>
            @endcan
            @can('pay-groups-list')
            <li data-menu="" class="{{ Request::segment(2) == 'payGroups'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/payGroups') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Pay Groups</a>
            </li>
            @endcan
            @can('announcements-list')
            <li data-menu="" class="{{ Request::segment(2) == 'announcements'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/announcements') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Announcements</a>
            </li>
            @endcan
{{--            @can('policy-list')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'policy'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/policy') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Company Policy</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
        </ul>
    </li>
    @endcan

    @can('timesheet')
    <li class="dropdown nav-item  {{ Request::segment(1) == 'timesheet'? 'active': '' }}" data-menu="dropdown">
        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
            <i class="menu-livicon" data-icon="clock"></i><span>Timesheet</span></a>

        <ul class="dropdown-menu">
            @can('attendance-list')
            <li data-menu="" class="{{ Request::segment(2) == 'attendance'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('timesheet/attendance') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Daily Attendance List</a>
            </li>
            @endcan
{{--            @can('monthly-timesheet')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'monthly-timesheet'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/monthly-timesheet') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Monthly Timesheet</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
            @can('attendance-update')
            <li data-menu="" class="{{ Request::segment(2) == 'attendance-update'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/attendance-update') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Attendance Update</a>
            </li>
            @endcan
            @can('import-attendance')
            <li data-menu="" class="{{ Request::segment(2) == 'import-attendance'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/import-attendance') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Import Attendance</a>
            </li>
            @endcan
{{--            @can('office-shifts')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'office-shifts'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/payGroups') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Office Shifts</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--            @can('holidays')--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'holidays'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/payGroups') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Holidays</a>--}}
{{--            </li>--}}
{{--            @endcan--}}
            @can('leaves')
            <li data-menu="" class="{{ Request::segment(2) == 'leaves'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/leaves') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Leaves</a>
            </li>
            <li data-menu="" class="{{ Request::segment(2) == 'leave-approval'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/leave-approval') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Leave Approval</a>
            </li>
            @endcan
            @can('overtime')
            <li data-menu="" class="{{ Request::segment(2) == 'overtime'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/overtime') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Overtime</a>
            </li>
            <li data-menu="" class="{{ Request::segment(2) == 'overtime-approval'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/overtime-approval') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Overtime Approval</a>
            </li>
            @endcan
            @can('official-business')
            <li data-menu="" class="{{ Request::segment(2) == 'ob'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/ob') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Official Business</a>
            </li>
            <li data-menu="" class="{{ Request::segment(2) == 'ob-approval'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('timesheet/ob-approval') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Official Business Approval</a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('payroll')
    <li class="dropdown nav-item  {{ Request::segment(1) == 'payroll'? 'active': '' }}" data-menu="dropdown">
        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
            <i class="menu-livicon" data-icon="calculator"></i><span>Payroll & Billing</span></a>

        <ul class="dropdown-menu">
            @can('process-dtr')
            <li data-menu="" class="{{ Request::segment(2) == 'dtr-process'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('payroll/dtr-process') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Process DTR</a>
            </li>
            @endcan
            @can('billing')
            <li data-menu="" class="{{ Request::segment(2) == 'billing'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('payroll/billing') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Billing</a>
            </li>
            @endcan
            @can('process-payroll')
            <li data-menu="" class="{{ Request::segment(2) == 'payroll-run'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('payroll/payroll-run') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Process Payroll</a>
            </li>
            @endcan
            @can('payroll-list')
            <li data-menu="" class="{{ Request::segment(2) == 'payroll-list'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('payroll/payroll-list') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Payroll List</a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan


    @can('report')
    <li class="dropdown nav-item  {{ Request::segment(1) == 'report'? 'active': '' }}" data-menu="dropdown">
        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
            <i class="menu-livicon" data-icon="notebook"></i><span>Reports</span></a>

        <ul class="dropdown-menu">
            <li data-menu="" class="{{ Request::segment(2) == 'attendance-report'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('organization/companies') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Attendance Report</a>
            </li>
{{--            <li data-menu="" class="{{ Request::segment(2) == 'process-payroll'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/costCenters') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Process Payroll</a>--}}
{{--            </li>--}}
{{--            <li data-menu="" class="{{ Request::segment(2) == 'payroll'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/departments') }}" data-toggle="dropdown">--}}
{{--                    <i class="bx bx-right-arrow-alt"></i>Payroll List</a>--}}
{{--            </li>--}}
        </ul>
    </li>
    @endcan

    @can('maintenance')
    <li class="dropdown nav-item  {{ Request::segment(1) == 'maintenance'? 'active': '' }}" data-menu="dropdown">
        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
            <i class="menu-livicon" data-icon="settings"></i><span>Maintenance</span></a>

        <ul class="dropdown-menu">
            <li data-menu="" class="{{ Request::segment(2) == 'roles'? 'active': '' }}"><a class="dropdown-item align-items-center" href="{{ url('maintenance/roles') }}" data-toggle="dropdown">
                    <i class="bx bx-right-arrow-alt"></i>Roles & Permission</a>
            </li>
            {{--            <li data-menu="" class="{{ Request::segment(2) == 'process-payroll'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/costCenters') }}" data-toggle="dropdown">--}}
            {{--                    <i class="bx bx-right-arrow-alt"></i>Process Payroll</a>--}}
            {{--            </li>--}}
            {{--            <li data-menu="" class="{{ Request::segment(2) == 'payroll'? 'active': '' }}" ><a class="dropdown-item align-items-center" href="{{ url('organization/departments') }}" data-toggle="dropdown">--}}
            {{--                    <i class="bx bx-right-arrow-alt"></i>Payroll List</a>--}}
            {{--            </li>--}}
        </ul>
    </li>
    @endcan
</ul>
