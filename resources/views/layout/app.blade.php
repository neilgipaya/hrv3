<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Aviary HR</title>
    <link rel="apple-touch-icon" href="{{ asset('app-assets') }}/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets') }}/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/extensions/dragula.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/editors/quill/katex.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/editors/quill/monokai-sublime.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/editors/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/vendors/css/editors/quill/quill.bubble.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/plugins/forms/validation/form-validation.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/plugins/forms/wizard.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/izitoast/css/iziToast.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/pages/page-users.css">

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    <!-- END: Custom CSS-->

    <style>
        table.dataTable.table-striped.DTFC_Cloned tbody tr:nth-of-type(odd) {
            background: #F3F3F3;
        }

        table.dataTable.table-striped.DTFC_Cloned tbody tr:nth-of-type(even) {
            background: white;
        }

        tr[role=row] {
            background:white;
        }
    </style>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns   footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed bg-primary navbar-brand-center">
    <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item"><a class="navbar-brand" href="{{ url('dashboard') }}">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Aviary HR</h2>
                </a></li>
        </ul>
    </div>
    <div class="navbar-wrapper">
        <div class="navbar-container content">

            @include('layout.navbar')

        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-header d-xl-none d-block">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="index.html">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Aviary HR</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <!-- Horizontal menu content-->
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include ../../../includes/mixins-->
        @include('layout.left_menu')
    </div>
    <!-- /horizontal menu content-->
</div>
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>

    @yield('content')

</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2019 &copy; Aviary</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="{{ asset('app-assets') }}/vendors/js/vendors.min.js"></script>
<script src="{{ asset('app-assets') }}/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="{{ asset('app-assets') }}/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="{{ asset('app-assets') }}/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets') }}/vendors/js/ui/jquery.sticky.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/pickers/pickadate/picker.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/pickers/daterange/moment.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/charts/apexcharts.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/extensions/dragula.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="{{ asset('app-assets/js/scripts/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/inputmask/bindings/inputmask.binding.js') }}"></script>
<script src="{{ asset('app-assets') }}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{ asset('app-assets/css/plugins/izitoast/js/iziToast.min.js') }}"></script>
<script src="{{ asset('app-assets') }}/vendors/js/editors/quill/katex.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/editors/quill/highlight.min.js"></script>
<script src="{{ asset('app-assets') }}/vendors/js/editors/quill/quill.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('app-assets') }}/js/scripts/configs/horizontal-menu.js"></script>
<script src="{{ asset('app-assets') }}/js/core/app-menu.js"></script>
<script src="{{ asset('app-assets') }}/js/core/app.js"></script>
<script src="{{ asset('app-assets') }}/js/scripts/components.js"></script>
<script src="{{ asset('app-assets') }}/js/scripts/footer.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
@yield('scripts')
<!-- END: Page JS-->
<script src="{{ asset('app-assets') }}/js/scripts/forms/validation/form-validation.js"></script>
<script src="{{ asset('app-assets') }}/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="{{ asset('app-assets') }}/js/scripts/editors/editor-quill.js"></script>

<script>


    @if(Session::has('message'))

    let type = "{{ Session::get('alert-type', 'info') }}";

    switch(type){

        case 'info':
            Swal.fire({

                type: 'info',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });

                break;

        case 'warning':

            Swal.fire({

                type: 'warning',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });

                break;

        case 'success':
            Swal.fire({

                type: 'success',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });
                break;

        case 'error':
            Swal.fire({

                type: 'error',
                title: "{{ Session::get('message') }}",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000

            });
            break;
    }
    @endif

</script>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script>
            toastr["warning"]("{{ $error }}");

            {{--new Noty({--}}
            {{--    type: 'warning',--}}
            {{--    layout: 'topRight',--}}
            {{--    text: "{{ $error }}"--}}
            {{--}).show();--}}
        </script>
    @endforeach
@endif

<script>
    // Basic Select2 select
    $(".s2").select2({
        placeholder: "Choose an Option",
        dropdownAutoWidth: true,
        width: '100%',
    });

</script>

<script>
    $(document).ready(function() {
        if (location.hash) {
            $("a[href='" + localStorage.getItem('location_hash') + "']").tab("show");
        }
        $(document.body).on("click", "a[data-toggle='tab']", function(event) {
            location.hash = this.getAttribute("href");
            localStorage.setItem('location_hash', location.hash);
        });
    });
    $(window).on("popstate", function() {
        let anchor = localStorage.getItem('location_hash') || $("a[data-toggle='tab']").first().attr("href");
        $("a[href='" + anchor + "']").tab("show");

    });

    let anchor = localStorage.getItem('location_hash') || $("a[data-toggle='tab']").first().attr("href");
    $("a[href='" + anchor + "']").tab("show");

</script>

<script>
    $('#sortTable').dataTable({
        // "ordering": false,
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select class="form-control"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    });
</script>

</body>
<!-- END: Body-->

</html>
