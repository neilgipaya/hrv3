@extends('layout.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Analytics Start -->
            <section id="dashboard-analytics">
                <div class="row">
                    Dashboard for member is still in progress
                </div>
            </section>
            <!-- Dashboard Analytics end -->

        </div>
    </div>

@endsection


@section('scripts')

    <script src="{{ asset('app-assets') }}/js/scripts/pages/dashboard-analytics.js"></script>

@endsection
