@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Form wizard with step validation section start -->
            <section id="validation">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">Create Employee</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="/employees" method="POST" class="wizard-validation">

                                        <!-- Step 1 -->
                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Employee Information</span>
                                        </h6>
                                        <!-- Step 1 -->
                                        <!-- body content of step 1 -->
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Employee ID</label>
                                                        <input type="text" class="form-control required" name="employee_id" placeholder="Employee ID" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>First Name </label>
                                                        <input type="text" class="form-control required" name="firstname" placeholder="Enter Your First Name" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control required" name="lastname" placeholder="Enter Your Last Name" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lastName3">Middle Name</label>
                                                        <input type="text" class="form-control required" name="middlename" placeholder="Enter Your Middle Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Gender</label>
                                                        <select class="form-control required" name="gender">
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Civil Status</label>
                                                        <select class="form-control required" name="civil_status">
                                                            <option value="Single">Single</option>
                                                            <option value="Married">Married</option>
                                                            <option value="Widowed">Widowed</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Date of Birth</label>
                                                        <input type="text" class="form-control required single-daterange" name="date_of_birth" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No</label>
                                                        <input type="text" class="form-control required" name="contact_no" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Home Address</label>
                                                        <textarea class="form-control required" name="home_address" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Work Email</label>
                                                        <input type="email" class="form-control required" name="email" placeholder="Enter Your Email" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="location">Username</label>
                                                        <input type="text" class="form-control required" name="username" placeholder="Enter Username" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input type="text" class="form-control required" name="password" placeholder="Enter Password" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Role</label>
                                                        <select class="form-control s2" name="role" required>
                                                            @foreach($roles as $role)
                                                                <option value="{{ $role->id }}"> {{ $role->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- body content of step 1 end -->
                                        <!-- Step 2 -->
                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Salary Details</span>
                                        </h6>
                                        <!-- step 2 -->
                                        <!-- body content of step 2 end -->
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Tax Status</label>
                                                        <select class="form-control required" name="tax_code">
                                                            <option value="S">Single</option>
                                                            <option value="X">Exempted</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Minimum Wage Earner?</label>
                                                        <select class="form-control required" name="is_minimum_earner">
                                                            <option value="No">No</option>
                                                            <option value="Yes">Yes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Cola</label>
                                                        <input type="text" class="form-control required money" name="cola" value="0" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Work Days Per Year</label>
                                                        <input type="text" class="form-control required number" name="work_days_per_year" value="365" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Basic Salary</label>
                                                        <input type="text" class="form-control required money" name="basic_salary" value="0" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">De Minimis</label>
                                                        <input type="text" class="form-control required money" name="de_minimis" value="0" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>OT Computation Code</label>
                                                        <select class="form-control" name="ot_computation_code">
                                                            <option value="0">No Computation</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>SSS Contribution</label>
                                                        <select class="form-control" name="sss_contribution">
                                                            <option value="0">Auto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Philhealth Contribution</label>
                                                        <select class="form-control" name="philhealth_contribution">
                                                            <option value="0">Auto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>HDMF Contribution</label>
                                                        <select class="form-control required" name="civil_status">
                                                            <option value="0">Auto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                        <!-- body content of step 2 end -->
                                        <!-- Step 3 -->
                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Job Details</span>
                                        </h6>
                                        <!-- step 3 end -->
                                        <!-- step 3 content -->
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Company</label>
                                                        <select class="form-control" name="companies_id" id="company" onchange="checkCompany()">
                                                            <option value="" selected>-- Select Company --</option>
                                                            @foreach($companies as $company)
                                                                <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Cost Center</label>
                                                        <select class="form-control" name="cost_centers_id" id="cost_center">

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Department</label>
                                                        <select class="form-control" name="departments_id" id="department">

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Job Title</label>
                                                        <input type="text" class="form-control required" name="job_title" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Hire Date</label>
                                                        <input type="text" class="form-control required single-daterange" name="hire_date" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Employment Status</label>
                                                        <select class="form-control required" name="employment_status">
                                                            <option value="Regular" selected>Regular</option>
                                                            <option value="Probationary">Probationary</option>
                                                            <option value="Contractual">Contractual</option>
                                                            <option value="End of Contract">End of Contract</option>
                                                            <option value="Part Time">Part Time</option>
                                                            <option value="Resigned / Terminated">Resigned / Terminated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Effectivity Date</label>
                                                        <input type="text" class="form-control required single-daterange" name="employment_effectivity_date" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Work hours per day</label>
                                                        <input type="text" class="form-control required number" name="work_hours_per_day" value="8" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Pay Groups</label>
                                                        <select class="form-control" name="pay_groups_id">
                                                            @foreach($pay_groups as $pay_group)
                                                                <option value="{{ $pay_group->id }}">{{ $pay_group->pg_name }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>TIN ID</label>
                                                        <input type="text" class="form-control required" name="tin_number" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Philhealth No.</label>
                                                        <input type="text" class="form-control required" name="philhealth_number" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>HDMF No.</label>
                                                        <input type="text" class="form-control required" name="hdmf_number" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>SSS No.</label>
                                                        <input type="text" class="form-control required" name="sss_number" />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- step 3 content end-->

                                        <h6>
                                            <i class="step-icon"></i>
                                            <span>Employment Information</span>
                                        </h6>

                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Vacation Leave</label>
                                                        <input type="text" class="form-control number" value="0" name="vl">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Sick Leave</label>
                                                        <input type="text" class="form-control number" value="0" name="sl">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Other Leave</label>
                                                        <input type="text" class="form-control number" value="0" name="other_leave">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/forms/wizard-steps.js"></script>


    <script>
        $(".money").inputmask({ alias : "currency" });
        $(".number").inputmask({ alias: "numeric" });


        function checkCompany() {

            let company = $("#company").val();

            let cost_center = $("#cost_center").empty();
            let department = $("#department").empty();


            $.get('/ajax/fetchCostCenter/' + company, function (response) {

                $('<option value="">' + "-- Select Record --" + '</option>').appendTo(cost_center);

                $.each(response, function(i, item) {
                    console.log(item['id']);
                    $('<option value="' + item['id'] + '">' + item['cost_center_name'] + '</option>').
                    appendTo(cost_center);
                });

            });

            $.get('/ajax/fetchDepartment/' + company, function (response) {

                $('<option value="">' + "-- Select Record --" + '</option>').appendTo(department);

                $.each(response, function(i, item) {
                    console.log(item['id']);
                    $('<option value="' + item['id'] + '">' + item['department_name'] + '</option>').
                    appendTo(department);
                });

            });
        }
    </script>
@endsection
