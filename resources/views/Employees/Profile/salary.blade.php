<div class="tab-pane" id="salary" aria-labelledby="information-tab" role="tabpanel">
    <!-- users edit Info form start -->
    <form action="/salary/{{ $employee->salaries->id }}" method="POST" novalidate>
        @csrf
        @method('PATCH')
        <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tax Status</label>
                        <select class="form-control required" name="tax_code">
                            <option value="S" {{ $employee->tax_code === "Single" }}>Single</option>
                            <option value="X" {{ $employee->tax_code === "X" }}>Exempted</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Minimum Wage Earner?</label>
                        <select class="form-control required" name="is_minimum_earner">
                            <option value="No" {{ $employee->is_minimum_earner === "No" }}>No</option>
                            <option value="Yes" {{ $employee->is_minimum_earner === "Yes" }}>Yes</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Cola</label>
                        <input type="text" class="form-control required money" name="cola" value="{{ $employee->salaries->cola }}" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Work Days Per Year</label>
                        <input type="text" class="form-control required number" name="work_days_per_year" value="{{ $employee->salaries->work_days_per_year }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Basic Salary</label>
                        <input type="text" class="form-control required money" name="basic_salary" value="{{ $employee->salaries->basic_salary }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">De Minimis</label>
                        <input type="text" class="form-control required money" name="de_minimis" value="{{ $employee->salaries->de_minimis }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>OT Computation Code</label>
                        <select class="form-control" name="ot_computation_code">
                            <option value="0">No Computation</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>SSS Contribution</label>
                        <select class="form-control" name="sss_contribution">
                            <option value="0">Auto</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Philhealth Contribution</label>
                        <select class="form-control" name="philhealth_contribution">
                            <option value="0">Auto</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>HDMF Contribution</label>
                        <select class="form-control required" name="civil_status">
                            <option value="0">Auto</option>
                        </select>
                    </div>
                </div>
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    changes</button>
                <button type="reset" class="btn btn-light">Cancel</button>
            </div>
        </div>
    </form>
    <!-- users edit Info form ends -->
</div>
