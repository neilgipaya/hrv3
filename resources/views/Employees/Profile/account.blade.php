<div class="tab-pane" id="account" aria-labelledby="information-tab" role="tabpanel">
    <!-- users edit Info form start -->
    <form action="/employees/{{ $employee->id }}" method="POST" novalidate>
        @csrf
        @method('PATCH')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" value="{{ $employee->username }}" readonly />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>New Password</label>
                    <input type="text" class="form-control required" name="password" placeholder="Enter new password to change" />
                </div>
            </div>
            @hasanyrole('Super Admin')
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Role</label>
                    <select class="form-control s2 required" name="role">
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ auth()->user()->roles->first()->id === $role->id ? "Selected" : "" }}> {{ $role->name }}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            @endhasanyrole

            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    changes</button>
                <button type="reset" class="btn btn-light">Cancel</button>
            </div>
        </div>
    </form>
    <!-- users edit Info form ends -->
</div>
