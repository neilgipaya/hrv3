<div class="tab-pane active" id="personal" aria-labelledby="account-tab" role="tabpanel">
    <!-- users edit media object start -->
    <div class="media mb-2">
        <a class="mr-2" href="#">
            <img src="{{ asset('app-assets') }}/images/portrait/small/avatar-s-26.jpg" alt="users avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Profile Picture</h4>
            <div class="col-12 px-0 d-flex">
                <a href="#" class="btn btn-sm btn-primary mr-25">Change</a>
                <a href="#" class="btn btn-sm btn-light-secondary">Reset</a>
            </div>
        </div>
    </div>
    <!-- users edit media object ends -->
    <!-- users edit account form start -->
    <form action="/employees/{{ $employee->id }}" method="POST" novalidate>
        @csrf
        @method('PATCH')
        <div class="row">
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <div class="controls">
                        <label>Last Name</label>
                        <input type="text" class="form-control" required value="{{ $employee->lastname }}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <label>First Name</label>
                        <input type="text" class="form-control" required value="{{ $employee->firstname }}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <label>Middle Name</label>
                        <input type="text" class="form-control" value="{{ $employee->middlename }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <label>Suffix</label>
                        <input type="text" class="form-control" name="suffix" value="{{ $employee->suffix }}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <label>Gender</label>
                        <select class="form-control" name="gender" required>
                            <option value="Male" {{ $employee->gender === "Male" ? "Selected" : "" }}>Male</option>
                            <option value="Female" {{ $employee->gender === "Female" ? "Selected" : "" }}>Female</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <div class="controls">
                        <label>Civil Status</label>
                        <select class="form-control required" name="civil_status">
                            <option value="">-- Select --</option>
                            <option value="Single" {{ $employee->civil_status === "Single" ? "Selected" : "" }}>Single</option>
                            <option value="Married" {{ $employee->civil_status === "Married" ? "Selected" : "" }}>Married</option>
                            <option value="Widowed" {{ $employee->civil_status === "Widowed" ? "Selected" : "" }}>Widowed</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Date of Birth</label>
                    <input type="text" class="form-control required single-daterange" name="date_of_birth" value="{{ $employee->date_of_birth }}" />
                </div>
                <div class="form-group">
                    <label>Contact No</label>
                    <input type="text" class="form-control required" name="contact_no" value="{{ $employee->contact_no }}" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control required" name="home_address" rows="3">{{ $employee->home_address }}</textarea>
                </div>
            </div>
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    Changes</button>
                <button type="reset" class="btn btn-light">Cancel</button>
            </div>
        </div>
    </form>
    <!-- users edit account form ends -->
</div>
