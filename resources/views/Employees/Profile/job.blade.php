<div class="tab-pane" id="job" aria-labelledby="information-tab" role="tabpanel">
    <!-- users edit Info form start -->

    <form action="/job-details/{{ $employee->jobDetails->id }}" method="POST" novalidate>
        @csrf
        @method('PATCH')
        <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company</label>
                        <select class="form-control" name="companies_id" id="company">
                            <option value="" selected>-- Select Company --</option>
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}" {{ $employee->jobDetails->companies_id  === $company->id ? "Selected" : "" }}>{{ $company->company_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cost Center</label>
                        <select class="form-control" name="cost_centers_id" id="cost_center">

                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Department</label>
                        <select class="form-control" name="departments_id" id="department">

                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Job Title</label>
                        <input type="text" class="form-control required" name="job_title" value="{{ $employee->jobDetails->job_title }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Hire Date</label>
                        <input type="text" class="form-control required single-daterange" name="hire_date" value="{{ $employee->jobDetails->hire_date }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Employment Status</label>
                        <select class="form-control required" name="employment_status">
                            <option value="Regular" selected {{ $employee->jobDetails->employment_status === "Regular" ? "Selected" : "" }}>Regular</option>
                            <option value="Probationary" {{ $employee->jobDetails->employment_status === "Probationary" ? "Selected" : "" }}>Probationary</option>
                            <option value="Contractual" {{ $employee->jobDetails->employment_status === "Contractual" ? "Selected" : "" }}>Contractual</option>
                            <option value="End of Contract" {{ $employee->jobDetails->employment_status === "End of Contract" ? "Selected" : "" }}>End of Contract</option>
                            <option value="Part Time" {{ $employee->jobDetails->employment_status === "Part Time" ? "Selected" : "" }}>Part Time</option>
                            <option value="Resigned / Terminated" {{ $employee->jobDetails->employment_status === "Resigned / Terminated" ? "Selected" : "" }}>Resigned / Terminated</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Effectivity Date</label>
                        <input type="text" class="form-control required single-daterange" name="employment_effectivity_date" value="{{ $employee->jobDetails->employment_effectivity_date }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Work hours per day</label>
                        <input type="text" class="form-control required number" name="work_hours_per_day" value="{{ $employee->jobDetails->work_hours_per_day }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Pay Groups</label>
                        <select class="form-control" name="pay_groups_id">
                            <option value="">-- Select --</option>
                            @foreach($pay_groups as $pay_group)
                                <option value="{{ $pay_group->id }}" {{ $employee->jobDetails->pay_groups_id === $pay_group->id ? "Selected" : "" }}>{{ $pay_group->pg_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>TIN ID</label>
                        <input type="text" class="form-control required" name="tin_number" value="{{ $employee->jobDetails->tin_number }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Philhealth No.</label>
                        <input type="text" class="form-control required" name="philhealth_number" value="{{ $employee->jobDetails->philhealth_number }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>HDMF No.</label>
                        <input type="text" class="form-control required" name="hdmf_number" value="{{ $employee->jobDetails->hdmf_number }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>SSS No.</label>
                        <input type="text" class="form-control required" name="sss_number" value="{{ $employee->jobDetails->sss_number }}" />
                    </div>
                </div>
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    changes</button>
                <button type="reset" class="btn btn-light">Cancel</button>
            </div>
        </div>
    </form>
    <!-- users edit Info form ends -->
</div>
