<div class="tab-pane" id="report" aria-labelledby="information-tab" role="tabpanel">
    <!-- users edit Info form start -->
    @can('report-to-add')
    <form method="POST" action="/reporting" novalidate>
        @csrf
        <p>Report to</p>
        <div class="row">
            <input type="hidden" class="form-control" value="{{ $employee->id }}" name="users_id" />
            <div class="col-md-6">
                <div class="form-group">
                    <select class="form-control" name="approver_id">
                        @foreach($employees as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->lastname . " " . $employee->firstname }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <select class="form-control s2" name="reporting_type" required>
                        <option value="Direct">Direct</option>
                        <option value="Indirect">Indirect</option>
                    </select>
                </div>
            </div>

            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    changes</button>
                <button type="reset" class="btn btn-light">Cancel</button>
            </div>
        </div>
    </form>
    @endcan
    <!-- users edit Info form ends -->

    <div class="row">
        <div class="col-md-12">
            <h5>Approvers</h5>
            <div class="table-responsive">
                <table class="table zero-configuration">
                    <thead>
                    <tr>
                        <th>Employee</th>
                        <th>Reporting Method</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($approvers as $row)
                        <tr>
                            <td>{{ $row->approver->lastname . " " . $row->approver->firstname }}</td>
                            <td>{{ $row->reporting_type }}</td>
                            <td>
                                <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                        data-toggle="modal" data-target="#update{{ $row->id }}">
                                    <i class="bx bx-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                        data-toggle="modal" data-target="#delete{{ $row->id }}">
                                    <i class="bx bx-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
