@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Employee List</h4>
                                </div>
                                <div style="float: right;">
                                    <a href="{{ url('employees/create') }}" class="btn btn-danger"> Create New</a>

                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Employee No</th>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Department</th>
                                                <th>Cost Center</th>
                                                <th>Date Hired</th>
                                                <th>Employment Status</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($employees as $row)
                                                <tr>
                                                    <td>{{ $row->id }}</td>
                                                    <td>{{ $row->employee_id }}</td>
                                                    <td>{{ $row->lastname . " " . $row->firstname . " " . $row->middlename[0] }}</td>
                                                    <td>{{ $row->jobDetails->job_title ?? "NOT AVAILABLE" }}</td>
                                                    <td>{{ $row->jobDetails->department->department_name ?? "NOT AVAILABLE" }}</td>
                                                    <td>{{ $row->jobDetails->costCenter->cost_center_name ?? "NOT AVAILABLE" }}</td>
                                                    <td>{{ $row->jobDetails->hire_date ?? "NOT AVAILABLE" }}</td>
                                                    <td>
                                                        {{ $row->jobDetails->employment_status ?? "NOT AVAILABLE" }}</td>
                                                    <td>
                                                        <a href="{{ url('employees/'.$row->id) }}">
                                                            <i class="bx bx-edit-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
