@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users edit start -->
            <section class="users-edit">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="nav nav-tabs mb-2" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#personal" aria-controls="account" role="tab" aria-selected="true">
                                        <i class="bx bx-user mr-25"></i><span class="d-none d-sm-block">Personal Information</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center" id="job-tab" data-toggle="tab" href="#job" aria-controls="information" role="tab" aria-selected="false">
                                        <i class="bx bx-building-house mr-25"></i><span class="d-none d-sm-block">Job Details</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center" id="salary-tab" data-toggle="tab" href="#salary" aria-controls="information" role="tab" aria-selected="false">
                                        <i class="bx bx-calculator mr-25"></i><span class="d-none d-sm-block">Salary Details</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center" id="report-tab" data-toggle="tab" href="#account" aria-controls="information" role="tab" aria-selected="false">
                                        <i class="bx bx-building-house mr-25"></i><span class="d-none d-sm-block">Account Settings</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center" id="report-tab" data-toggle="tab" href="#report" aria-controls="information" role="tab" aria-selected="false">
                                        <i class="bx bx-building-house mr-25"></i><span class="d-none d-sm-block">Report to</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                @include('Employees.Profile.personal')
                                @include('Employees.Profile.job')
                                @include('Employees.Profile.salary')
                                @include('Employees.Profile.account')
                                @include('Employees.Profile.report-to')
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- users edit ends -->
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/pages/page-users.js"></script>
    <script src="{{ asset('app-assets') }}/js/scripts/navs/navs.js"></script>

    <script src="{{ asset('app-assets') }}/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>

    <script>
        $(".money").inputmask({ alias : "currency" });
        $(".number").inputmask({ alias: "numeric" });


        let company = $("#company").val();

        let cost_center = $("#cost_center").empty();
        let department = $("#department").empty();


        $.get('/ajax/fetchCostCenter/' + company, function (response) {

            $.each(response, function(i, item) {

                let current = "{{ $employee->jobDetails->costCenter->id }}";
                let selected = "";

                if (current == item['id']){
                    selected = "selected";
                } else {
                    selected = "";
                }


                $('<option value="' + item['id'] + '"' + selected +'>' + item['cost_center_name'] + '</option>').
                appendTo(cost_center);
            });

        });

        $.get('/ajax/fetchDepartment/' + company, function (response) {

            $('<option value="">' + "-- Select Record --" + '</option>').appendTo(department);

            $.each(response, function(i, item) {
                console.log(item['id']);
                let current_department = "{{ $employee->jobDetails->department->id }}";
                let selected = "";

                if (current_department == item['id']){
                    selected = "selected";
                } else {
                    selected = "";
                }

                $('<option value="' + item['id'] + '"' + selected +'>' + item['department_name'] + '</option>').
                appendTo(department);

            });

        });
    </script>
    <script>

    </script>
@endsection
