@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">{{ ucfirst(request()->segment('1')) }}</h5>
                        @if(!empty(request()->segment('2')))
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">{{ ucfirst(request()->segment('2')) }}
                                    </li>
                                </ol>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Announcements List</h4>
                                </div>
                                @can('announcements-create')
                                <div style="float: right;">
                                    <a href="#" data-toggle="modal" data-target="#create" class="btn btn-primary"> Create New</a>

                                </div>
                                @endcan
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Title</th>
                                                <th>Content</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Attachment</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($announcements as $row)
                                                <tr>
                                                    <td>{{ $row->company->company_name }}</td>
                                                    <td>{{ $row->announcement_title }}</td>
                                                    <td>{{ $row->announcement_description }}</td>
                                                    <td>{{ $row->start_date }}</td>
                                                    <td>{{ $row->end_date }}</td>
                                                    <td>{{ $row->attachment }}</td>
                                                    <td>
                                                        @can('announcements-edit')
                                                        <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                data-toggle="modal" data-target="#update{{ $row->id }}">
                                                            <i class="bx bx-pencil"></i>
                                                        </button>
                                                        @endcan
                                                        @can('announcements-delete')
                                                        <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                            <i class="bx bx-trash"></i>
                                                        </button>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    <!--primary theme Modal -->
    <div class="modal fade text-left" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">

                <form action="/organization/announcements" method="POST">
                    @csrf

                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">Create Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Companies</label>
                            <select class="form-control" name="companies_id">
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="announcement_title" />
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" name="announcement_description" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input type="text" class="form-control required single-daterange" name="start_date" />
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input type="text" class="form-control required single-daterange" name="end_date" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Cancel</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Save</span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    @foreach($announcements as $row)
        <div class="modal fade text-left" id="update{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/organization/announcements/{{ $row->id }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Companies</label>
                                <select class="form-control" name="companies_id">
                                    @foreach($companies as $company)
                                        <option value="{{ $company->id }}" {{ $company->id === $row->company_id ? "Selected" : "" }}>{{ $company->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" name="announcement_title" value="{{ $row->announcement_title }}" />
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="announcement_description" rows="4">{{ $row->announcement_description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="text" class="form-control required single-daterange" name="start_date" value="{{ $row->start_date }}" />
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="text" class="form-control required single-daterange" name="end_date" value="{{ $row->end_date }}" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-warning ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Save Changes</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($announcements as $row)
        <div class="modal fade text-left" id="delete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/organization/companies/{{ $row->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
