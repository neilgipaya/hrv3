@extends('layout.app')

@section('content')

    <div class="content-wrapper">

        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Organization</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">Companies
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div style="float: left">
                                    <h4 class="card-title" >Pay Groups</h4>
                                </div>
                                <div style="float: right;">
                                    <a href="#" data-toggle="modal" data-target="#create" class="btn btn-primary"> Create New</a>

                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($pay_groups as $row)
                                                <tr>
                                                    <td>{{ $row->pg_code }}</td>
                                                    <td>{{ $row->pg_name }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon rounded-circle btn-warning"
                                                                data-toggle="modal" data-target="#update{{ $row->id }}">
                                                            <i class="bx bx-pencil"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-icon rounded-circle btn-danger"
                                                                data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                            <i class="bx bx-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>

    <!--primary theme Modal -->
    <div class="modal fade text-left" id="create" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">

                <form action="/organization/payGroups" method="POST">
                    @csrf

                    <div class="modal-header bg-primary">
                        <h5 class="modal-title white" id="myModalLabel160">Create Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Pay Group Code</label>
                            <input type="text" class="form-control required" name="pg_code" />
                        </div>
                        <div class="form-group">
                            <label>Pay Group Name</label>
                            <input type="text" class="form-control required" name="pg_name" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Cancel</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Save</span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    @foreach($pay_groups as $row)
        <div class="modal fade text-left" id="update{{$row->id}}" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/organization/payGroups/{{ $row->id }}" method="POST" novalidate>
                        @csrf
                        @method('PATCH')
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title white" id="myModalLabel160">Update Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Pay Group Code</label>
                                <input type="text" class="form-control required" value="{{ $row->pg_code }}" name="pg_code" />
                            </div>
                            <div class="form-group">
                                <label>Pay Group Name</label>
                                <input type="text" class="form-control required" value="{{ $row->pg_name }}" name="pg_name" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-warning ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Save Changes</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

    @foreach($pay_groups as $row)
        <div class="modal fade text-left" id="delete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">

                    <form action="/organization/payGroups/{{ $row->id }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <div class="modal-header bg-danger">
                            <h5 class="modal-title white" id="myModalLabel160">Delete Record</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to remove this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-danger ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Delete</span>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
    <script src="{{ asset('app-assets') }}/js/scripts/datatables/datatable.js"></script>
@endsection
